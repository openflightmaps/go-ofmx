package types

type LbmUid struct {
	RegionalUid
	CodeType string `xml:"codeType" json:"codeType" validaton:"oneof=TEXT FIS-INFO"`
	TxtName  string `xml:"txtName" json:"txtName"`
}

func (uid *LbmUid) String() string {
	return UidString(*uid)
}

func (uid *LbmUid) Hash() string {
	return UidHash(*uid)
}

type Lbm struct {
	LbmUid        LbmUid `xml:"LbmUid" json:"LbmUid"`
	TxtValueLabel string `xml:"txtValueLabel" json:"txtValueLabel"`
	LatLong
	CodeDatum string      `xml:"codeDatum" json:"codeDatum"`
	ZoomLevel []ZoomLevel `xml:"ZoomLevel" json:"ZoomLevel"`
	validation
}

func (f *Lbm) Uid() FeatureUid {
	return &f.LbmUid
}

type ZoomLevel struct {
	ValZoomLevel  string `xml:"valZoomLevel" json:"valZoomLevel"`
	TxtValueLabel string `xml:"txtValueLabel,omitempty" json:"txtValueLabel,omitempty"`
	GeoLat        string `xml:"geoLat,omitempty" json:"geoLat,omitempty" validate:"lat,omitempty"`
	GeoLong       string `xml:"geoLong,omitempty" json:"geoLong,omitempty" validate:"lon,omitempty"`
	validation
}
