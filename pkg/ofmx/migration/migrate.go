package migration

import (
	"fmt"
	"io"
	"log"

	"gitlab.com/openflightmaps/go-ofmx/internal/pkg/migration/legacyfixups"
	"gitlab.com/openflightmaps/go-ofmx/internal/pkg/migration/v01fixups"
	"gitlab.com/openflightmaps/go-ofmx/internal/pkg/migration/xmlparserfixups"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
)

type Migrator interface {
	// check if version can be migrated, if version is not known the hdr can be provided instead and version left empty
	CanMigrate(version string, hdr *ofmx.OfmxHeader) bool
	TargetVersion() string
	MigrateSnapshot(snap *ofmx.OfmxSnapshot) (*ofmx.OfmxSnapshot, error)
	MigrateReader(r io.Reader) (io.Reader, error)
}

type multiMigrator struct {
	migrators []Migrator
	Migrator
}

func (mm multiMigrator) CanMigrate(version string, hdr *ofmx.OfmxHeader) bool {
	return mm.migrators[0].CanMigrate(version, hdr)
}

func (mm multiMigrator) TargetVersion() string {
	return mm.migrators[len(mm.migrators)-1].TargetVersion()
}

func (mm multiMigrator) MigrateSnapshot(snap *ofmx.OfmxSnapshot) (*ofmx.OfmxSnapshot, error) {
	var err error
	for _, m := range mm.migrators {
		log.Printf("Migrating from %s to %s", snap.Version, m.TargetVersion())
		snap, err = m.MigrateSnapshot(snap)
		if err != nil {
			return nil, err
		}
		// update version
		snap.Version = m.TargetVersion()
	}
	return snap, err
}

func (mm multiMigrator) MigrateReader(r io.Reader) (io.Reader, error) {
	var err error
	for _, m := range mm.migrators {
		r, err = m.MigrateReader(r)
		if err != nil {
			return nil, err
		}
	}
	return r, err
}

func NewMigrator(hdr *ofmx.OfmxHeader, targetVersion string) (Migrator, error) {
	if targetVersion == "" {
		targetVersion = "0.1"
	}

	// TODO: fix header XML parsing errors
	_, err := xmlparserfixups.New().MigrateHeader(hdr)
	if err != nil {
		return nil, err
	}

	migrators := []Migrator{
		legacyfixups.New(),
		v01fixups.New(),
	}

	version := ""
	h := hdr
	mm := multiMigrator{}
	for _, m := range migrators {
		if m.CanMigrate(version, h) {
			version = m.TargetVersion()
			h = nil
			mm.migrators = append(mm.migrators, m)
		}
	}

	if len(mm.migrators) > 0 && mm.migrators[len(mm.migrators)-1].TargetVersion() == targetVersion {
		return mm, nil
	}
	return nil, fmt.Errorf("baseVersion (%s) / targetVersion (%s) combination not supported", hdr.Version, targetVersion)
}
