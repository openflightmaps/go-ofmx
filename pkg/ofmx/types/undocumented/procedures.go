package undocumented

import "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"

// TODO: UNDOCUMENTED

type PrcUid struct {
	types.Uid
	AhpUid *types.AhpUid `xml:"AhpUid" json:"AhpUid"`
	// RwyUid *types.RwyUid `xml:"RwyUid,omitempty" validate:"isdefault" json:"RwyUid,omitempty" hash:"optional"`
	// RdnUid *types.RdnUid `xml:"RdnUid,omitempty" validate:"isdefault" json:"RdnUid,omitempty" hash:"optional"`

	CodeId string `xml:"codeId,omitempty" json:"codeId,omitempty" validate:"omitempty"`
	// TODO

	// TxtName  string `xml:"txtName,omitempty" json:"txtName,omitempty"`
	validation
}

func (uid *PrcUid) String() string {
	return types.UidString(*uid)
}

func (uid *PrcUid) Hash() string {
	return types.UidHash(*uid)
}

type GmlPath struct {
	GmlPosList string `xml:"gmlPosList,omitempty" json:"gmlPosList,omitempty"`
}

// TODO
type LegNode struct {
	CodeType string        `xml:"codeType" json:"codeType"`
	DpnUid   *types.DpnUid `xml:"DpnUid" json:"DpnUid" validate:"omitempty"`
	VorUid   *types.VorUid `xml:"VorUid" json:"VorUid" validate:"omitempty"`
	RdnUid   *types.RdnUid `xml:"RdnUid" json:"RdnUid" validate:"omitempty"`
	NdbUid   *types.NdbUid `xml:"NdbUid" json:"NdbUid" validate:"omitempty"`
	PrcUid   *PrcUid       `xml:"PrcUid" json:"PrcUid" validate:"omitempty"`

	CodeDistVerUpper string `xml:"codeDistVerUpper,omitempty" json:"codeDistVerUpper,omitempty"`
	ValDistVerUpper  string `xml:"valDistVerUpper,omitempty" json:"valDistVerUpper,omitempty"`
	UomDistVerUpper  string `xml:"uomDistVerUpper,omitempty" json:"uomDistVerUpper,omitempty"`
	CodeDistVerLower string `xml:"codeDistVerLower,omitempty" json:"codeDistVerLower,omitempty"`
	ValDistVerLower  string `xml:"valDistVerLower,omitempty" json:"valDistVerLower,omitempty"`
	UomDistVerLower  string `xml:"uomDistVerLower,omitempty" json:"uomDistVerLower,omitempty"`

	// for codeType TRAFFIC_CIRCUIT only
	TfcDir string `xml:"tfcDir,omitempty" json:"tfcDir,omitempty"`

	validation
}

type Sector struct {
	Id        string   `xml:"id" json:"id"`
	CodeType  string   `xml:"codeType,omitempty" json:"codeType,omitempty"`
	X         string   `xml:"x,omitempty" json:"x,omitempty"`
	Y         string   `xml:"y,omitempty" json:"y,omitempty"`
	VisThr    string   `xml:"visThr,omitempty" json:"visThr,omitempty"`
	GeoBounds *GmlPath `xml:"geoBounds,omitempty" json:"geoBounds,omitempty"`
	validation
}

type Leg struct {
	Id     string   `xml:"id" json:"id"`
	Type   string   `xml:"type" json:"type" validate:"oneof=FIX_TO_FIX FIX_TO_FIX_OVERFLY FIX_TO_FIX_OVERFLY_NS FIX_TO_FIX_OVERFLY_DCT"`
	Entry  *LegNode `xml:"entry,omitempty" json:"entry,omitempty" validate:"omitempty"`
	Exit   *LegNode `xml:"exit,omitempty" json:"exit,omitempty" validate:"omitempty"`
	Sector []Sector `xml:"sector,omitempty" json:"sector,omitempty"`
	validation
}

type Prc struct {
	PrcUid                 PrcUid  `xml:"PrcUid" json:"PrcUid"`
	TxtName                string  `xml:"txtName,omitempty" json:"txtName,omitempty"`
	TxtNameAbbr            string  `xml:"txtNameAbbr,omitempty" json:"txtNameAbbr,omitempty"`
	TxtRmk                 string  `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	CodeType               string  `xml:"codeType,omitempty" json:"codeType,omitempty" validate:"oneof=VFR_HOLD TRAFFIC_CIRCUIT VFR_TRANS VFR_ARR VFR_DEP IFR_TRANS"`
	UsageType              string  `xml:"usageType,omitempty" json:"usageType,omitempty" validate:"oneof=FIXED_WING HELICOPTER"`
	BezTrajectory          GmlPath `xml:"_beztrajectory,omitempty" json:"_beztrajectory,omitempty" validate:"dive"`
	BeztrajectoryAlternate GmlPath `xml:"_beztrajectoryAlternate,omitempty" json:"_beztrajectoryAlternate,omitempty" validate:"dive"`
	SceletonPath           GmlPath `xml:"_sceletonPath,omitempty" json:"_sceletonPath,omitempty" validate:"dive"`

	// VFR_HOLD
	HoldingEntryTxtName string `xml:"HoldingEntry_txtName,omitempty" json:"HoldingEntry_txtName,omitempty"`
	HoldingEntryGeoLat  string `xml:"holdingEntryGeoLat,omitempty" json:"holdingEntryGeoLat,omitempty"`
	HoldingEntryGeoLong string `xml:"holdingEntryGeoLong,omitempty" json:"holdingEntryGeoLong,omitempty"`
	HoldingInboundTrack string `xml:"holdingInboundTrack,omitempty" json:"holdingInboundTrack,omitempty"`
	HoldingOrientation  string `xml:"holdingOrientation,omitempty" json:"holdingOrientation,omitempty"`

	// TRAFFIC_CIRCUIT / VFR_HOLD
	TfcShapePoints *GmlPath `xml:"tfc_shapePoints,omitempty" json:"tfc_shapePoints,omitempty" validate:"omitempty,dive"`

	// Vertical Limits
	CodeDistVerTfc string `xml:"codeDistVerTfc,omitempty" json:"codeDistVerTfc,omitempty"`
	UomDistVerTfc  string `xml:"uomDistVerTfc,omitempty" json:"uomDistVerTfc,omitempty"`
	ValDistVerTfc  string `xml:"valDistVerTfc,omitempty" json:"valDistVerTfc,omitempty"`

	Leg []Leg `xml:"Leg,omitempty" json:"Leg,omitempty" validate:"dive"`
	validation
}

func (f *Prc) Uid() types.FeatureUid {
	return &f.PrcUid
}
