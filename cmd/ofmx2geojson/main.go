// TODO: Remove
//
//nolint:forbidigo
package main

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"log"
	"os"

	"golang.org/x/net/html/charset"

	geojson "github.com/paulmach/go.geojson"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
	types "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types/undocumented"
)

func main() {
	var snap ofmx.OfmxSnapshot

	b, err := os.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}

	decoder := xml.NewDecoder(bytes.NewReader(b))
	decoder.Strict = true
	decoder.CharsetReader = charset.NewReaderLabel

	err = decoder.Decode(&snap)
	if err != nil {
		panic("Cannot parse input file:" + err.Error())
	}

	fmap := types.NewFeatureMap()
	if err = fmap.AddFeatureList(&snap.Features); err != nil {
		panic("Cannot parse input file:" + err.Error())
	}
	ofmx.UpdateReferences(fmap)

	fmt.Println(len(fmap.Slice()))

	gjOther := geojson.NewFeatureCollection()
	gjDpn := geojson.NewFeatureCollection()
	gjAhp := geojson.NewFeatureCollection()
	gjAbd := geojson.NewFeatureCollection()
	gjPrc := geojson.NewFeatureCollection()
	gjFir := geojson.NewFeatureCollection()
	gjMsc := geojson.NewFeatureCollection()
	for _, f := range snap.Features.Slice() {
		var j *geojson.Feature

		// silently ignore non-geo-features
		switch t := f.(type) {
		// added using references
		case *types.Aha:
			// DONE
			continue
		case *types.Ful:
			// DONE
			continue
		case *types.Ahu:
			// DONE
			continue

		case *types.Aga:
			// TODO, add to relevant
			continue
		case *types.Ahs:
			// TODO, these are missing in XML
			continue

			// until here
			// ----
			// services
		case *types.Fqy:
			// TODO add to relevant
			continue
		case *types.Sae:
			// TODO add to relevant
			continue
		case *types.Ser:
			// TODO add to relevant
			continue
		case *types.Uni:
			// TODO add to relevant
			continue
		case *types.Org:
			// TODO add to relevant, added to Ahp
			continue

			// end services

			// airspaces
		case *types.Abd:
			// TODO Discuss, how and if this should be added
			continue
		case *types.Adg:
			// TODO Discuss, how and if this should be added
			continue

			// end airspaces

			// airports
		case *types.Rwy:
			// TODO
			continue
		case *types.Rdd:
			// TODO
			continue
		case *types.Rdn:
			// TODO
			continue
		case *types.Rls:
			// TODO
			continue
		case *types.Tls:
			// TODO
			continue
		case *types.Tla:
			// TODO
			continue

			// heliports
		case *types.Fto:
			// TODO
			continue
		case *types.Fdd:
			// TODO
			continue
		case *types.Fdn:
			// TODO
			continue

			// uncoducmented
		case *undocumented.Twy:
			// TODO
			continue
		case *undocumented.Tly:
			// TODO
			continue
		case *undocumented.Plp:
			continue
		case *undocumented.Ppa:
			continue

			// obstacles
		case *types.Obs:
			// TODO, add later
			continue
		case *types.Ogr:
			// TODO, add later
			continue

			// TODO
		case *types.Ase:
			if t.AseUid.CodeType == "FIR" {
				log.Println("FIR:", t.AseUid.CodeId)
			}
		}

		gg, ok := f.(types.GeoFeatureGeometry)
		if ok {
			g, err := gg.GeoJsonGeometry(fmap)
			if err != nil {
				fmt.Printf("Error, skipping: %s, %s\n", f.Uid().String(), err)
				continue
			}

			j = geojson.NewFeature(g)
			err = types.FillProperties(gg, j)
			if err != nil {
				fmt.Printf("Error, skipping: %s, %s\n", f.Uid().String(), err)
				continue
			}
		} else {
			gf, ok := f.(types.GeoFeature)
			if !ok {
				fmt.Printf("Not a GeoFeature, skipping: %s\n", f.Uid().String())
				panic("not a geofeature")
			}
			j, err = gf.GeoJson(fmap)
			if err != nil {
				fmt.Printf("Error, skipping: %s, %s\n", f.Uid().String(), err)
				continue
			}
			if err = types.FillProperties(gf, j); err != nil {
				fmt.Printf("Error, skipping: %s, %s\n", f.Uid().String(), err)
				continue
			}
		}

		switch t := f.(type) {
		case *types.Dpn:
			gjDpn.AddFeature(j)
			// gjAll.AddFeature(j)

		case *types.Ahp:
			gjAhp.AddFeature(j)
			// gjAll.AddFeature(j)
		case *types.Ase:
			// gjAll.AddFeature(j)
			gjAbd.AddFeature(j)
			if t.AseUid.CodeType == "FIR" || t.AseUid.CodeType == "CTA" {
				gjFir.AddFeature(j)
			}
		case *types.Gbr:
			// ignore
			// case *types.Ase:
			// 	gjAll.AddFeature(j)
		case *undocumented.Msc:
			gjMsc.AddFeature(j)
		case *undocumented.Apn:
			gjAhp.AddFeature(j)

		case *undocumented.Prc:
			gjPrc.AddFeature(j)

		default:
			gjOther.AddFeature(j)
			continue
		}
	}
	js, _ := gjDpn.MarshalJSON()
	if err = os.WriteFile("navigation.geojson", js, 0o644); err != nil {
		panic(err)
	}
	js, _ = gjMsc.MarshalJSON()
	if err = os.WriteFile("misc.geojson", js, 0o644); err != nil {
		panic(err)
	}
	js, _ = gjAbd.MarshalJSON()
	if err = os.WriteFile("airports.geojson", js, 0o644); err != nil {
		panic(err)
	}
	js, _ = gjAbd.MarshalJSON()
	if err = os.WriteFile("airspace.geojson", js, 0o644); err != nil {
		panic(err)
	}
	js, _ = gjPrc.MarshalJSON()
	if err = os.WriteFile("procedures.geojson", js, 0o644); err != nil {
		panic(err)
	}
	js, _ = gjFir.MarshalJSON()
	if err = os.WriteFile("firs.geojson", js, 0o644); err != nil {
		panic(err)
	}
	js, _ = gjOther.MarshalJSON()
	if err = os.WriteFile("other.geojson", js, 0o644); err != nil {
		panic(err)
	}
}
