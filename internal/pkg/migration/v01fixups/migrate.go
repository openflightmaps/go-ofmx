package v01fixups

import (
	"io"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types/undocumented"
)

/*
  - TODO: DME/VOR
    codeChannel is a required field ”
	codeTypeNorth must be one of [TRUE MAG GRID UTM OTHER] 'M'

	Timsh: xt_AlignSummerSavingT

	Ase: valDistVerUpper – Upper vertical limit (use "STD 999 FL" for UNLIMITED) / UNL

	Fqy: xt_primary

	PlpUid||||: Key: 'Plp.basemapOpacity' Error:Field validation for 'basemapOpacity' failed on the 'isdefault' tag '0.5'
	PlpUid||||: Key: 'Plp.terrainOpacity' Error:Field validation for 'terrainOpacity' failed on the 'isdefault' tag '0'

    PlpUid||||: UnknownTags should not exist according to OFMX specification! Please file a bug report '[{{ plateIdentifier} 2-1}]'

	XT_surface twy
*/

func New() v01FixupMigrator {
	return v01FixupMigrator{}
}

type v01FixupMigrator struct{}

func (m v01FixupMigrator) MigrateSnapshot(snap *ofmx.OfmxSnapshot) (*ofmx.OfmxSnapshot, error) {
	for _, f := range snap.Features.Slice() {
		var err error
		switch t := f.(type) {
		case *types.Fdn:
			err = migrateFdn(t)
		case *types.Ase:
			err = migrateAse(t)
		case *types.Ndb:
			err = migrateNdb(t)
		case *types.Dpn:
			err = migrateDpn(t)
		case *undocumented.Prc:
			err = migratePrc(t)
		}
		if err != nil {
			return nil, err
		}
	}
	return snap, nil
}

func (m v01FixupMigrator) MigrateReader(r io.Reader) (io.Reader, error) {
	return r, nil
}

func (m v01FixupMigrator) CanMigrate(version string, hdr *ofmx.OfmxHeader) bool {
	// we support version 0.1 prerelease (output from previous migrator)
	if version == "0.1-pre" {
		return true
	}

	if version != "" || hdr == nil {
		return false
	}

	if hdr.Version == "0.1" && hdr.NoNamespaceSchemaLocation == "https://schema.openflightmaps.org/0.1/OFMX-Snapshot.xsd" {
		return true
	}
	return false
}

func (m v01FixupMigrator) TargetVersion() string {
	return "0.1"
}
