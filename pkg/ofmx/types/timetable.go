package types

import (
	"fmt"
	"time"
)

type Timetable struct {
	CodeWorkHr   string  `xml:"codeWorkHr,omitempty" validate:"omitempty,oneof=H24 HJ HN HX HO NOTAM OTHER TIMSH" json:"codeWorkHr,omitempty"`
	Timsh        []Timsh `xml:"Timsh,omitempty" validate:"dive" json:"Timsh,omitempty"`
	TxtRmkWorkHr string  `xml:"txtRmkWorkHr,omitempty" json:"txtRmkWorkHr,omitempty"`
	validation
}

type Timsh struct {
	CodeTimeRef      string `xml:"codeTimeRef" validate:"oneof=UTC UTCW" json:"codeTimeRef"`
	DateValidWef     string `xml:"dateValidWef" validate:"omitempty,date" json:"dateValidWef"`
	DateYearValidWef string `xml:"dateYearValidWef,omitempty" validate:"omitempty,year" json:"dateYearValidWef,omitempty"`
	DateValidTil     string `xml:"dateValidTil" validate:"omitempty,date" json:"dateValidTil"`
	DateYearValidTil string `xml:"dateYearValidTil,omitempty" validate:"omitempty,year" json:"dateYearValidTil,omitempty"`
	CodeDay          string `xml:"codeDay" validate:"oneof=MON TUE WED THU FRI SAT SUN WD PWD AWD LH PLH ALH ANY" json:"codeDay"`
	CodeDayTil       string `xml:"codeDayTil,omitempty" validate:"oneof=MON TUE WED THU FRI SAT SUN WD PWD AWD LH PLH ALH ANY" json:"codeDayTil,omitempty"`
	TimeWef          string `xml:"timeWef,omitempty" validate:"omitempty,time" json:"timeWef,omitempty"`
	CodeEventWef     string `xml:"codeEventWef,omitempty" validate:"omitempty,oneof=SR SS" json:"codeEventWef,omitempty"`
	TimeRelEventWef  string `xml:"timeRelEventWef,omitempty" validate:"omitempty,numeric" json:"timeRelEventWef,omitempty"`
	CodeCombWef      string `xml:"codeCombWef,omitempty" validate:"omitempty,oneof=E L" json:"codeCombWef,omitempty"`
	TimeTil          string `xml:"timeTil,omitempty" validate:"omitempty,time" json:"timeTil,omitempty"`
	CodeEventTil     string `xml:"codeEventTil,omitempty" validate:"omitempty,oneof=SR SS" json:"codeEventTil,omitempty"`
	TimeRelEventTil  string `xml:"timeRelEventTil,omitempty" validate:"omitempty,numeric" json:"timeRelEventTil,omitempty"`
	CodeCombTil      string `xml:"codeCombTil,omitempty" validate:"omitempty,oneof=E L" json:"codeCombTil,omitempty"`
	validation
}

const dateLayout = "02-01-2006 15:04"

func parseOfmxDateTime(dateS, yearS, timeS string) (*time.Time, error) {
	// end-of-day support 24:00
	eod := false
	if timeS == "24:00" {
		eod = true
		timeS = "00:00"
	}
	s := fmt.Sprintf("%s-%s %s", dateS, yearS, timeS)
	tim, err := time.Parse(dateLayout, s)
	if eod {
		tim = tim.AddDate(0, 0, 1)
	}
	return &tim, err
}

func (t *Timsh) EffectiveFrom() (*time.Time, error) {
	if t.CodeTimeRef != "UTC" {
		return nil, fmt.Errorf("unsupported CodeTimeRef %s", t.CodeTimeRef)
	}
	if t.CodeDay != "ANY" {
		return nil, fmt.Errorf("unsupported CodeDay %s", t.CodeDay)
	}

	return parseOfmxDateTime(t.DateValidWef, t.DateYearValidWef, t.TimeWef)
}

func (t *Timsh) ValidUntil() (*time.Time, error) {
	if t.CodeTimeRef != "UTC" {
		return nil, fmt.Errorf("unsupported CodeTimeRef %s", t.CodeTimeRef)
	}
	if t.CodeDay != "ANY" {
		return nil, fmt.Errorf("unsupported CodeDay %s", t.CodeDay)
	}
	return parseOfmxDateTime(t.DateValidTil, t.DateYearValidTil, t.TimeTil)
}
