//nolint:forbidigo
package main

import (
	"bytes"
	"flag"
	"fmt"
	"os"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/migration"
	valid "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/validation"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
)

func createUniSvcMap(fmap types.FeatureMap) {
	options := make(map[string]bool, 0)
	for _, f := range fmap.Slice() {
		switch t := f.(type) {
		case *types.Uni:
		//	fmt.Printf("UNI %s %v\n", k, t.UniUid.CodeType)
		case *types.Ser:
			combo := fmt.Sprintf("%s/%s", t.SerUid.CodeType, t.SerUid.UniUid.CodeType)
			options[combo] = true
			fmt.Printf("Ser %s %s %s\n", f.Uid().String(), t.SerUid.CodeType, t.SerUid.UniUid.CodeType)
		}
	}
	fmt.Println("Ser/Uid combinations")
	for k := range options {
		fmt.Println(k)
	}
}

func main() {
	snap := &ofmx.OfmxSnapshot{}
	var errorList []valid.ValidationError
	validate := valid.New()
	// validate := validator.New()
	// en := en.New()
	// uni := ut.New(en, en)
	// trans, _ := uni.GetTranslator("en")
	// en_translations.RegisterDefaultTranslations(validate, trans)
	// validation.RegisterValidators(validate, trans)

	fmap := types.NewFeatureMap()

	filename := flag.String("filename", "", "ofmx to load")
	ignoreUndocumented := flag.Bool("ignore-undocumented", false, "ignore undocumented features")
	flag.Parse()

	if *filename == "" {
		flag.Usage()
		os.Exit(1)
	}
	b, err := os.ReadFile(*filename)
	if err != nil {
		panic(err)
	}

	opts := make([]ofmx.DecoderOption, 0)
	opts = append(opts, ofmx.MidMode(types.MID_MODE_CLIENT))
	if !*ignoreUndocumented {
		opts = append(opts, ofmx.AllowUndocumented())
	}
	decoder := ofmx.NewDecoder(bytes.NewReader(b), opts...)

	err = decoder.Decode(snap)
	if err != nil {
		panic("Cannot parse input file:" + err.Error())
	}
	mig, err := migration.NewMigrator(&snap.OfmxHeader, "0.1")
	if err != nil {
		panic(err)
	}
	snap, err = mig.MigrateSnapshot(snap)
	if err != nil {
		panic(err)
	}
	fmt.Println("after migration")
	if err = fmap.AddFeatureList(&snap.Features); err != nil {
		panic(err)
	}
	ofmx.UpdateReferences(fmap)
	createUniSvcMap(fmap)

	for _, f := range snap.Features.Slice() {
		uid := f.Uid()

		mid := f.Uid().OriginalMid()
		if uid.Hash() != mid {
			fmt.Printf("%s: %s/%s\n", uid.String(), uid.Hash(), mid)
		}
		errors := validate.ValidateFeature(f)
		if errors != nil {
			errorList = append(errorList, errors...)
		}
	}

	for _, e := range errorList {
		switch e.Tag {
		case "undocumented":
			// fmt.Printf("%s: UNDOCUMENTED: %s '%v'\n", e.Id, e.Error, e.Value)
		default:
			fmt.Printf("%s: %s '%v'\n", e.Id, e.Error, e.Value)
		}
	}
}
