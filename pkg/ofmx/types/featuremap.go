package types

import (
	"fmt"
)

type featureMap struct {
	fm map[string]Feature
}

type FeatureMap interface {
	ByUid(uid FeatureUid) (Feature, error)
	AddFeatures(features ...Feature) error
	AddFeatureList(features FeatureList) error
	AddFeatureMap(features FeatureMap) error
	Slice() []Feature
}

func (fmap featureMap) ByUid(uid FeatureUid) (Feature, error) {
	f, ok := fmap.fm[uid.Hash()]
	if !ok {
		// log.Printf("could not find %s", uid.String())
		return nil, fmt.Errorf("could not find %s", uid.String())
	}
	return f, nil
}

func (fmap featureMap) AddFeatureMap(features FeatureMap) error {
	return fmap.AddFeatures(features.Slice()...)
}

func (fmap featureMap) AddFeatureList(features FeatureList) error {
	return fmap.AddFeatures(features.Slice()...)
}

func (fmap featureMap) AddFeatures(features ...Feature) error {
	for _, f := range features {
		_, ok := fmap.fm[f.Uid().Hash()]
		if ok {
			// TODO: this should be an error
			//nolint:forbidigo
			fmt.Printf("Duplicate feature: %s\n", f.Uid().String())
		}
		fmap.fm[f.Uid().Hash()] = f
	}
	return nil
}

func (fmap featureMap) Slice() []Feature {
	res := make([]Feature, 0, len(fmap.fm))
	for _, v := range fmap.fm {
		res = append(res, v)
	}
	return res
}

// TODO: add options for hashing functions, duplicate handling...
func NewFeatureMap() FeatureMap {
	return &featureMap{
		fm: make(map[string]Feature, 0),
	}
}
