package ofmx

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewFeature(t *testing.T) {
	f, err := NewFeature("Fqy", false)
	assert.NotNil(t, f)
	assert.Nil(t, err)
}
