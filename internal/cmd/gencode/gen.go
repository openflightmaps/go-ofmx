package main

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/printer"
	"go/token"
	"log"
	"os"
	"strings"

	"github.com/fatih/structtag"
)

func main() {
	// parse file
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, os.Args[2], nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}

	ast.Inspect(node, func(n ast.Node) bool {
		s, ok := n.(*ast.StructType)
		if ok {
			fmt.Printf("Struct found on line %d\n", fset.Position(s.Pos()).Line)
			for _, f := range s.Fields.List {
				if f.Tag == nil {
					continue
				}
				tag := f.Tag.Value
				tag = strings.Trim(tag, "`")
				fmt.Println(tag)
				tags, err := structtag.Parse(tag)
				if err != nil {
					panic(err)
				}
				// get a single tag
				xmlTag, err := tags.Get("xml")
				if err != nil {
					fmt.Println(err)
					continue
				}
				// get a single tag
				jsonTag, err := tags.Get("json")
				if err != nil {
					jsonTag := &structtag.Tag{
						Key:     "json",
						Name:    xmlTag.Name,
						Options: xmlTag.Options,
					}
					// no json tag, set to xml tag
					tags.Set(jsonTag)
					f.Tag.Value = fmt.Sprintf("`%s`", tags.String())
				}
				//fmt.Println(xmlTag.Value())
				fmt.Println(jsonTag)
				fmt.Println(tags)
			}
		}

		return true
	})
	// write new ast to file
	f, err := os.Create(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	if err := printer.Fprint(f, fset, node); err != nil {
		log.Fatal(err)
	}
}
