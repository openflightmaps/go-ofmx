package legacyfixups

import (
	"errors"
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

func formatLonOfmx(x float64) string {
	suffix := "E"
	if x < 0 {
		suffix = "W"
	}
	s := strconv.FormatFloat(math.Abs(x), 'f', 8, 64)
	for len(s) < 12 {
		s = "0" + s
	}
	return fmt.Sprintf("%s%s", s, suffix)
}

func formatLatOfmx(x float64) string {
	suffix := "N"
	if x < 0 {
		suffix = "S"
	}
	s := strconv.FormatFloat(math.Abs(x), 'f', 8, 64)
	for len(s) < 11 {
		s = "0" + s
	}
	return fmt.Sprintf("%s%s", s, suffix)
}

//nolint:unused
func parseGmlPoint(s string) (lat, lon float64, err error) {
	sp := strings.Split(s, " ")
	if len(sp) != 2 {
		return 0, 0, fmt.Errorf("invalid gml point: %v", s)
	}
	if lat, err = strconv.ParseFloat(sp[0], 64); err != nil {
		return
	}
	if lon, err = strconv.ParseFloat(sp[1], 64); err != nil {
		return
	}
	return
}

func convertFromDMS(f float64) float64 {
	d := math.Floor(f / 10000)
	m := math.Mod(math.Floor(f/100), 100)
	s := math.Mod(f, 100)

	r := math.Round((d + m/60 + s/3600) * 1000)
	return r / 1000
}

func parseCoord(in string, dms bool) (float64, error) {
	if len(in) == 0 {
		return 0, nil
	}
	v := in[:len(in)-1]
	d := string(in[len(in)-1])

	dir := 1.0
	if d == "W" || d == "S" {
		dir = -1.0
	}
	f, _ := strconv.ParseFloat(v, 64)

	if dms {
		log.Println("HACK, converting from DMS")
		f = convertFromDMS(f)
	}

	if f > 180 {
		log.Println("invalid coord:" + in)
		return f * dir, errors.New("invalid coord:" + in)
	}
	return f * dir, nil
}

func fixupCoord(lat, lon string) (string, string, error) {
	dms := false
	if len(lat) == 7 {
		dms = true
	}
	flat, err := parseCoord(lat, dms)
	if err != nil {
		return "", "", err
	}
	if len(lon) == 8 {
		dms = true
	}
	flon, err := parseCoord(lon, dms)
	if err != nil {
		return "", "", err
	}
	return formatLatOfmx(flat), formatLonOfmx(flon), nil
}

func migrateGbr(t *types.Gbr) error {
	for _, gbv := range t.Gbv {
		if gbv.CodeDatum == "" {
			gbv.CodeDatum = "WGE"
		} else if gbv.CodeDatum == "WGC" {
			gbv.CodeDatum = "WGE"
		}
		var err error
		if gbv.GeoLat, gbv.GeoLong, err = fixupCoord(gbv.GeoLat, gbv.GeoLong); err != nil {
			return err
		}
	}
	return nil
}
