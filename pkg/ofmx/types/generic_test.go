package types

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUidHash(t *testing.T) {
	uid := VorUid{RegionalUid: RegionalUid{Region: "LF"}, CodeId: "AGN", LatLong: LatLong{GeoLat: "435316.90N", GeoLong: "0005222.30E"}}

	assert.Equal(t, "VorUid|LF|AGN|435316.90N|0005222.30E", UidString(uid))
	assert.Equal(t, "fe6e3186-80aa-7c48-4557-edc84d9419f8", UidHash(uid))
}
