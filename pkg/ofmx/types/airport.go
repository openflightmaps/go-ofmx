package types

const (
	CODE_USAGE_LIMITATION_PERMIT = "PERMIT"
	CODE_USAGE_LIMITATION_FORBID = "FORBID"
)

// Ahp - Airport

type AhpUid struct {
	RegionalUid
	CodeId string `xml:"codeId" json:"codeId"`
}

func (uid *AhpUid) String() string {
	return UidString(*uid)
}

func (uid *AhpUid) Hash() string {
	return UidHash(*uid)
}

func (uid *AhpUid) Ref() *Ahp {
	if uid.IRef == nil {
		// TODO, needed? BUGON
		return nil
	}
	return uid.IRef.(*Ahp)
}

type Ahp struct {
	Source
	AhpUid   AhpUid `xml:"AhpUid" json:"AhpUid"`
	OrgUid   OrgUid `xml:"OrgUid" json:"OrgUid"`
	TxtName  string `xml:"txtName" json:"txtName"`
	CodeIcao string `xml:"codeIcao,omitempty" json:"codeIcao,omitempty"`
	CodeIata string `xml:"codeIata,omitempty" json:"codeIata,omitempty"`
	CodeGps  string `xml:"codeGps,omitempty" json:"codeGps,omitempty"`
	CodeType string `xml:"codeType" json:"codeType"`
	LatLong
	CodeDatum        string      `xml:"codeDatum" json:"codeDatum"`
	ValElev          string      `xml:"valElev,omitempty" json:"valElev,omitempty"`
	UomDistVer       string      `xml:"uomDistVer,omitempty" json:"uomDistVer,omitempty"`
	TxtNameCitySer   string      `xml:"txtNameCitySer,omitempty" json:"txtNameCitySer,omitempty"`
	ValMagVar        string      `xml:"valMagVar,omitempty" json:"valMagVar,omitempty"`
	DateMagVar       string      `xml:"dateMagVar,omitempty" json:"dateMagVar,omitempty"`
	ValMagVarChg     string      `xml:"valMagVarChg,omitempty" json:"valMagVarChg,omitempty"`
	ValRefT          string      `xml:"valRefT,omitempty" json:"valRefT,omitempty"`
	UomRefT          string      `xml:"uomRefT,omitempty" json:"uomRefT,omitempty"`
	TxtNameAdmin     string      `xml:"txtNameAdmin,omitempty" json:"txtNameAdmin,omitempty"`
	ValTransitionAlt string      `xml:"valTransitionAlt,omitempty" json:"valTransitionAlt,omitempty"`
	UomTransitionAlt string      `xml:"uomTransitionAlt,omitempty" json:"uomTransitionAlt,omitempty"`
	Aht              []Timetable `xml:"Aht,omitempty" json:"Aht,omitempty"`
	TxtRmk           string      `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation

	// References
	Ahu *Ahu   `xml:"-" json:"_Ahu,omitempty"`
	Aha []*Aha `xml:"-" json:"_Aha,omitempty"`
	Ahs []*Ahs `xml:"-" json:"_Ahs,omitempty"`
	Ful []*Ful `xml:"-" json:"_Ful,omitempty"`
	Org *Org   `xml:"-" json:"_Org,omitempty"`
}

func (f *Ahp) Uid() FeatureUid {
	return &f.AhpUid
}

// Rwy - Runway

type RwyUid struct {
	Uid

	AhpUid   *AhpUid `xml:"AhpUid" json:"AhpUid"`
	TxtDesig string  `xml:"txtDesig" json:"txtDesig"`
}

func (uid *RwyUid) String() string {
	return UidString(*uid)
}

func (uid *RwyUid) Hash() string {
	return UidHash(*uid)
}

func (uid *RwyUid) Ref() *Rwy {
	if uid.IRef == nil {
		// TODO, needed? BUGON
		return nil
	}
	return uid.IRef.(*Rwy)
}

type SurfaceCharacteristics struct {
	CodeComposition         string `xml:"codeComposition,omitempty" json:"codeComposition,omitempty" validate:"omitempty,oneof=ASPH BITUM CONC GRAVE MACADAM SAND GRADE GRASS WATER OTHER"`
	CodePreparation         string `xml:"codePreparation,omitempty" json:"codePreparation,omitempty" validate:"omitempty,oneof=NATURAL ROLLED GRADE GROOVED OILED PAVED PFC AFSC RFSC OTHER"`
	CodeCondSfc             string `xml:"codeCondSfc,omitempty" json:"codeCondSfc,omitempty" validate:"omitempty,oneof=GOOD FAIR POOR OTHER"`
	ValPcnClass             string `xml:"valPcnClass,omitempty" json:"valPcnClass,omitempty" validate:"omitempty,numeric"`
	CodePcnPavementType     string `xml:"codePcnPavementType,omitempty" json:"codePcnPavementType,omitempty" validate:"omitempty,oneof=F R"`
	CodePcnPavementSubgrade string `xml:"codePcnPavementSubgrade,omitempty" json:"codePcnPavementSubgrade,omitempty" validate:"omitempty,oneof=A B C D"`
	CodePcnMaxTirePressure  string `xml:"codePcnMaxTirePressure,omitempty" json:"codePcnMaxTirePressure,omitempty" validate:"omitempty,oneof=W X Y Z"`
	CodePcnEvalMethod       string `xml:"codePcnEvalMethod,omitempty" json:"codePcnEvalMethod,omitempty" validate:"omitempty,oneof=T U"`
	TxtPcnNote              string `xml:"txtPcnNote,omitempty" json:"txtPcnNote,omitempty" validate:"omitempty"`
	ValSiwlWeight           string `xml:"valSiwlWeight,omitempty" json:"valSiwlWeight,omitempty" validate:"omitempty,numeric"`
	UomSiwlWeight           string `xml:"uomSiwlWeight,omitempty" json:"uomSiwlWeight,omitempty" validate:"omitempty,oneof=KG T LB TON"`
	ValSiwlTirePressure     string `xml:"valSiwlTirePressure,omitempty" json:"valSiwlTirePressure,omitempty" validate:"omitempty,numeric"`
	UomSiwlTirePressure     string `xml:"uomSiwlTirePressure,omitempty" json:"uomSiwlTirePressure,omitempty" validate:"omitempty,oneof=P MPA PSI BAR TORR"`
	ValAuwWeight            string `xml:"valAuwWeight,omitempty" json:"valAuwWeight,omitempty" validate:"omitempty,numeric"`
	UomAuwWeight            string `xml:"uomAuwWeight,omitempty" json:"uomAuwWeight,omitempty" validate:"omitempty,oneof=KG T LB TON"`
	validation
}

type Rwy struct {
	RwyUid    RwyUid `xml:"RwyUid" json:"RwyUid"`
	ValLen    string `xml:"valLen,omitempty" json:"valLen,omitempty"`
	ValWid    string `xml:"valWid,omitempty" json:"valWid,omitempty"`
	UomDimRwy string `xml:"uomDimRwy,omitempty" json:"uomDimRwy,omitempty"`
	SurfaceCharacteristics
	ValLenStrip string `xml:"valLenStrip,omitempty" json:"valLenStrip,omitempty"`
	ValWidStrip string `xml:"valWidStrip,omitempty" json:"valWidStrip,omitempty"`
	UomDimStrip string `xml:"uomDimStrip,omitempty" json:"uomDimStrip,omitempty"`
	CodeSts     string `xml:"codeSts,omitempty" json:"codeSts,omitempty"`
	TxtRmk      string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Rwy) Uid() FeatureUid {
	return &f.RwyUid
}

// Rdn - Runway Direction

type RdnUid struct {
	// TODO, temp allow mid
	Uid

	RwyUid   *RwyUid `xml:"RwyUid" json:"RwyUid"`
	TxtDesig string  `xml:"txtDesig" json:"txtDesig"`
}

func (uid *RdnUid) String() string {
	return UidString(*uid)
}

func (uid *RdnUid) Hash() string {
	return UidHash(*uid)
}

func (uid *RdnUid) Ref() *Rdn {
	if uid.IRef == nil {
		// TODO, needed? BUGON
		return nil
	}
	return uid.IRef.(*Rdn)
}

type Rdn struct {
	RdnUid  RdnUid `xml:"RdnUid" json:"RdnUid"`
	GeoLat  string `xml:"geoLat" json:"geoLat"`
	GeoLong string `xml:"geoLong" json:"geoLong"`

	ValTrueBrg           string `xml:"valTrueBrg,omitempty" json:"valTrueBrg,omitempty"`
	ValMagBrg            string `xml:"valMagBrg,omitempty" json:"valMagBrg,omitempty"`
	ValElevTdz           string `xml:"valElevTdz,omitempty" json:"valElevTdz,omitempty"`
	UomElevTdz           string `xml:"uomElevTdz,omitempty" json:"uomElevTdz,omitempty"`
	CodeTypeVasis        string `xml:"codeTypeVasis,omitempty" json:"codeTypeVasis,omitempty"`
	CodePsnVasis         string `xml:"codePsnVasis,omitempty" json:"codePsnVasis,omitempty"`
	NoBoxVasis           string `xml:"noBoxVasis,omitempty" json:"noBoxVasis,omitempty"`
	CodePortableVasis    string `xml:"codePortableVasis,omitempty" json:"codePortableVasis,omitempty"`
	ValSlopeAngleGpVasis string `xml:"valSlopeAngleGpVasis,omitempty" json:"valSlopeAngleGpVasis,omitempty"`
	ValMeht              string `xml:"valMeht,omitempty" json:"valMeht,omitempty"`
	UomMeht              string `xml:"uomMeht,omitempty" json:"uomMeht,omitempty"`
	CodeVfrPattern       string `xml:"codeVfrPattern,omitempty" json:"codeVfrPattern,omitempty"`
	TxtRmk               string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation

	// References
	Rdd []*Rdd `xml:"-" validate:"-" json:"-"`
}

func (f *Rdn) Uid() FeatureUid {
	return &f.RdnUid
}

// Rdd - Runway Direction Declared Distance

type RddUid struct {
	Uid

	RdnUid        *RdnUid `xml:"RdnUid" json:"RdnUid"`
	CodeType      string  `xml:"codeType" json:"codeType"`
	CodeDayPeriod string  `xml:"codeDayPeriod" json:"codeDayPeriod"`
}

func (uid *RddUid) String() string {
	return UidString(*uid)
}

func (uid *RddUid) Hash() string {
	return UidHash(*uid)
}

type Rdd struct {
	RddUid  RddUid `xml:"RddUid" json:"RddUid"`
	ValDist string `xml:"valDist" json:"valDist"`
	UomDist string `xml:"uomDist" json:"uomDist"`
	TxtRmk  string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Rdd) Uid() FeatureUid {
	return &f.RddUid
}

// Rls - Runway Direction Lighting

type RlsUid struct {
	Uid

	RdnUid  *RdnUid `xml:"RdnUid" json:"RdnUid"`
	CodePsn string  `xml:"codePsn" json:"codePsn"`
}

func (uid *RlsUid) String() string {
	return UidString(*uid)
}

func (uid *RlsUid) Hash() string {
	return UidHash(*uid)
}

func (uid *RlsUid) Region() string {
	return uid.RdnUid.RwyUid.AhpUid.Region
}

type Rls struct {
	RlsUid     RlsUid `xml:"RlsUid" json:"RlsUid"`
	TxtDescr   string `xml:"txtDescr,omitempty" json:"txtDescr,omitempty"`
	CodeIntst  string `xml:"codeIntst,omitempty" json:"codeIntst,omitempty"`
	CodeColour string `xml:"codeColour,omitempty" json:"codeColour,omitempty"`
	TxtRmk     string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Rls) Uid() FeatureUid {
	return &f.RlsUid
}

// Rda - Runway Direction approach Lighting
type RdaUid struct {
	Uid
	RdnUid   RdnUid `xml:"RdnUid" json:"RdnUid"`
	CodeType string `xml:"codeType" json:"codeType"`
}

func (uid *RdaUid) String() string {
	return UidString(*uid)
}

func (uid *RdaUid) Hash() string {
	return UidHash(*uid)
}

type Rda struct {
	RdaUid             RdaUid `xml:"RdaUid" json:"RdaUid"`
	ValLen             string `xml:"valLen,omitempty" json:"valLen,omitempty"`
	UomLen             string `xml:"uomLen,omitempty" json:"uomLen,omitempty"`
	CodeIntst          string `xml:"codeIntst,omitempty" json:"codeIntst,omitempty"`
	CodeSequencedFlash string `xml:"codeSequencedFlash,omitempty" json:"codeSequencedFlash,omitempty"`
	TxtDescrFlash      string `xml:"txtDescrFlash,omitempty" json:"txtDescrFlash,omitempty"`
	TxtRmk             string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Rda) Uid() FeatureUid {
	return &f.RdaUid
}

// Ahu - Airport Usage

type AhuUid struct {
	// TODO: temp allow mid
	Uid

	AhpUid *AhpUid `xml:"AhpUid" json:"-"`
}

func (uid *AhuUid) String() string {
	return UidString(*uid)
}

func (uid *AhuUid) Hash() string {
	return UidHash(*uid)
}

type Ahu struct {
	AhuUid          AhuUid            `xml:"AhuUid" json:"AhuUid"`
	UsageLimitation []UsageLimitation `xml:"UsageLimitation" json:"UsageLimitation"`
	validation
}

func (f *Ahu) Uid() FeatureUid {
	return &f.AhuUid
}

type UsageLimitation struct {
	CodeUsageLimitation string           `xml:"codeUsageLimitation" json:"codeUsageLimitation"`
	UsageCondition      []UsageCondition `xml:"UsageCondition,omitempty" json:"UsageCondition,omitempty"`
	Timetable           []Timetable      `xml:"Timetable,omitempty" json:"Timetable,omitempty"`
	TxtRmk              string           `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

type UsageCondition struct {
	AircraftClass []AircraftClass `xml:"AircraftClass" json:"AircraftClass"`
	FlightClass   []FlightClass   `xml:"FlightClass" json:"FlightClass"`
	validation
}

type AircraftClass struct {
	CodeType string `xml:"codeType" json:"codeType"`
	validation
}

type FlightClass struct {
	CodeRule    string `xml:"codeRule,omitempty" json:"codeRule,omitempty"`
	CodeMil     string `xml:"codeMil,omitempty" json:"codeMil,omitempty"`
	CodeOrigin  string `xml:"codeOrigin,omitempty" json:"codeOrigin,omitempty"`
	CodePurpose string `xml:"codePurpose,omitempty" json:"codePurpose,omitempty"`
	validation
}

// Ahs - Airport Ground Service
type AhsUid struct {
	// TODO, temp allow mid
	Uid

	AhpUid   *AhpUid `xml:"AhpUid" json:"AhpUid"`
	CodeType string  `xml:"codeType" json:"codeType"`
}

func (uid *AhsUid) String() string {
	return UidString(*uid)
}

func (uid *AhsUid) Hash() string {
	return UidHash(*uid)
}

type Ahs struct {
	AhsUid      AhsUid    `xml:"AhsUid" json:"AhsUid"`
	TxtDescrFac string    `xml:"txtDescrFac" json:"txtDescrFac"`
	Ast         Timetable `xml:"Ast" json:"Ast"`
	TxtRmk      string    `xml:"txtRmk" json:"txtRmk"`
	validation

	// References
	Aga []*Aga `xml:"-" validate:"-" json:"-"`
}

func (f *Ahs) Uid() FeatureUid {
	return &f.AhsUid
}

// Aga - Airport Ground Service Address

type AgaUid struct {
	Uid

	AhsUid   *AhsUid `xml:"AhsUid" json:"AhsUid"`
	CodeType string  `xml:"codeType" json:"codeType"`
	NoSeq    string  `xml:"noSeq" json:"noSeq"`
}

func (uid *AgaUid) String() string {
	return UidString(*uid)
}

func (uid *AgaUid) Hash() string {
	return UidHash(*uid)
}

type Aga struct {
	AgaUid     AgaUid `xml:"AgaUid" json:"AgaUid"`
	TxtAddress string `xml:"txtAddress" json:"txtAddress"`
	TxtRmk     string `xml:"txtRmk" json:"txtRmk"`
	validation
}

func (f *Aga) Uid() FeatureUid {
	return &f.AgaUid
}

// Ful - Fuel

type FulUid struct {
	// TODO, temp allow mid
	Uid

	AhpUid  *AhpUid `xml:"AhpUid" json:"-"` // do not export backlink TODO
	CodeCat string  `xml:"codeCat" json:"codeCat"`
	validation
}

func (uid *FulUid) String() string {
	return UidString(*uid)
}

func (uid *FulUid) Hash() string {
	return UidHash(*uid)
}

type Ful struct {
	FulUid FulUid `xml:"FulUid" json:"FulUid"`

	TxtDescr string `xml:"txtDescr,omitempty" json:"txtDescr,omitempty"`
	TxtRmk   string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Ful) Uid() FeatureUid {
	return &f.FulUid
}

// Aha - Airport Address

type AhaUid struct {
	Uid

	AhpUid   *AhpUid `xml:"AhpUid" json:"AhpUid"`
	CodeType string  `xml:"codeType" json:"codeType"`
	NoSeq    int     `xml:"noSeq" json:"noSeq"`
}

func (uid *AhaUid) String() string {
	return UidString(*uid)
}

func (uid *AhaUid) Hash() string {
	return UidHash(*uid)
}

type Aha struct {
	Source
	AhaUid     AhaUid `xml:"AhaUid" json:"AhaUid"`
	TxtAddress string `xml:"txtAddress,omitempty" json:"txtAddress,omitempty"`
	TxtRmk     string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Aha) Uid() FeatureUid {
	return &f.AhaUid
}

// Sah - Airport Service

type SahUid struct {
	Uid
	AhpUid *AhpUid `xml:"AhpUid" json:"AhpUid"`
	SerUid *SerUid `xml:"SerUid" json:"SerUid"`
}

func (uid *SahUid) String() string {
	return UidString(*uid)
}

func (uid *SahUid) Hash() string {
	return UidHash(*uid)
}

type Sah struct {
	Source
	SahUid SahUid `xml:"SahUid" json:"SahUid"`
	validation
}

func (f *Sah) Uid() FeatureUid {
	return &f.SahUid
}
