package types

import (
	"encoding/xml"
	"time"
)

type XmlTime time.Time

type ParseError struct {
	Errors  []error
	Message string
}

func (e ParseError) Error() string {
	return e.Message
}

const (
	rfc3339UTCWithTZ = "2006-01-02T15:04:05Z07:00"
	rfc3339UTCWithZ  = "2006-01-02T15:04:05Z"
	rfc3339UTC       = "2006-01-02T15:04:05"
	rfc3339UTCBroken = "2006-01-02T15.04.05"
)

func (t *XmlTime) MarshalXMLAttr(name xml.Name) (xml.Attr, error) {
	if t == nil {
		return xml.Attr{Name: name, Value: ""}, nil
	}
	dateString := time.Time(*t).Format(rfc3339UTC)
	attr := xml.Attr{
		Name:  name,
		Value: dateString,
	}

	return attr, nil
}

func (t *XmlTime) UnmarshalXMLAttr(attr xml.Attr) error {
	if parse, err := time.Parse(rfc3339UTCWithTZ, attr.Value); err == nil {
		*t = XmlTime(parse)
		return nil
	}

	if parse, err := time.Parse(rfc3339UTC, attr.Value); err == nil {
		*t = XmlTime(parse)
		return nil
	}

	if parse, err := time.Parse(rfc3339UTCWithZ, attr.Value); err == nil {
		*t = XmlTime(parse)
		return nil
	}

	// TODO: This is an unsupported format and should throw an error instead, needed for upgrades
	if parse, err := time.Parse(rfc3339UTCBroken, attr.Value); err == nil {
		*t = XmlTime(parse)
		return nil
	} else {
		return err
	}
}
