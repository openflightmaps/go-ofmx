package main

import (
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/utils"
)

func main() {
	//filename := flag.String("filename", "test.types", "types file to check")

	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Println("usage: parsertest file.types")
		os.Exit(1)
	}
	fmt.Println()

	if err := processFile(flag.Arg(0), "test-out.types"); err != nil {
		panic(err)
	}
	fmt.Println("Verification successful")
}

func processFile(inFile, outFile string) error {
	var snap ofmx.OfmxSnapshot
	fmap := types.NewFeatureMap()

	f, err := os.Open(inFile)
	if err != nil {
		panic(err)
	}

	decoder := ofmx.NewDecoder(f, ofmx.MidMode(types.MID_MODE_CLIENT), ofmx.AllowUndocumented())

	err = decoder.Decode(&snap)
	if err != nil {
		panic("Cannot parse input file:" + err.Error())
	}
	fmap.AddFeatureList(&snap.Features)
	//types.UpdateReferences(fmap)

	o, err := os.Create(outFile)
	if err != nil {
		panic(err)
	}
	defer o.Close()
	if _, err = o.Write([]byte(xml.Header)); err != nil {
		panic(err)
	}

	encoder := xml.NewEncoder(o)
	encoder.Indent("", "  ")
	err = encoder.Encode(snap)
	if err != nil {
		panic("Cannot write output file:" + err.Error())
	}

	// re-parse XML
	f.Seek(0, io.SeekStart)
	hashOriginal := utils.HashXML(f, true)

	// reload temp xml
	o.Seek(0, io.SeekStart)
	hashRewritten := utils.HashXML(o, true)

	if len(hashOriginal) != len(hashRewritten) {
		return errors.New("number of nodes in XML do not match anymore")
	}

	for i := range hashRewritten {
		a := strings.Trim(hashOriginal[i], " \n\r\t")
		b := strings.Trim(hashRewritten[i], " \n\r\t")

		if a != b {
			//return errors.New(hashOriginal[i] + "\n" + hashRewritten[i])
			//fmt.Println("MISMATCH")
			fmt.Println(hashOriginal[i] + "\n" + hashRewritten[i])
			err = fmt.Errorf("at least one mismatch found")
		}
	}
	return err
}
