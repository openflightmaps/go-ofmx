package types

import "encoding/xml"

type XmlTag struct {
	XMLName xml.Name
	Content string `xml:",innerxml" json:"-"`
}

type validation struct {
	Comment     string   `xml:",comment" json:"-" hash:"ignore"`
	UnknownTags []XmlTag `xml:",any" json:"-" validate:"bug"`
	// UnknownAttributes []xml.Attr `xml:",any,attr" json:"-" validate:"bug"`
}
