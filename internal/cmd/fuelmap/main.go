package main

import (
	"bytes"
	"fmt"
	"os"

	flatten "github.com/doublerebel/bellows"

	geojson "github.com/paulmach/go.geojson"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

type Airport struct {
	ahp  types.Ahp
	fuel []types.Ful
}

func main() {
	var snap ofmx.OfmxSnapshot
	fmap := types.NewFeatureMap()
	fnames := []string{
		"test/testdata/ofmx/ofmx/ed/latest/isolated/ofmx_ed.xml",
		"test/testdata/ofmx/ofmx/fa/latest/isolated/ofmx_fa.xml",
		"test/testdata/ofmx/ofmx/lhcc/latest/isolated/ofmx_lh.xml",
		"test/testdata/ofmx/ofmx/ekdk/latest/isolated/ofmx_ek.xml",
		"test/testdata/ofmx/ofmx/lovv/latest/isolated/ofmx_lo.xml",
		"test/testdata/ofmx/ofmx/lzbb/latest/isolated/ofmx_lz.xml",
		"test/testdata/ofmx/ofmx/lbsr/latest/isolated/ofmx_lb.xml",
		"test/testdata/ofmx/ofmx/efin/latest/isolated/ofmx_ef.xml",
		"test/testdata/ofmx/ofmx/lkaa/latest/isolated/ofmx_lk.xml",
		"test/testdata/ofmx/ofmx/lsas/latest/isolated/ofmx_ls.xml",
		"test/testdata/ofmx/ofmx/ehaa/latest/isolated/ofmx_eh.xml",
		"test/testdata/ofmx/ofmx/epww/latest/isolated/ofmx_ep.xml",
		"test/testdata/ofmx/ofmx/ebbu/latest/isolated/ofmx_eb.xml",
		"test/testdata/ofmx/ofmx/lrbb/latest/isolated/ofmx_lr.xml",
		"test/testdata/ofmx/ofmx/li/latest/isolated/ofmx_li.xml",
		"test/testdata/ofmx/ofmx/ldzo/latest/isolated/ofmx_ld.xml",
		"test/testdata/ofmx/ofmx/ljla/latest/isolated/ofmx_lj.xml",
		"test/testdata/ofmx/ofmx/fywh/latest/isolated/ofmx_fy.xml",
		"test/testdata/ofmx/ofmx/esaa/latest/isolated/ofmx_es.xml",
	}

	for _, fname := range fnames {

		b, err := os.ReadFile(fname)
		if err != nil {
			panic(err)
		}

		decoder := ofmx.NewDecoder(bytes.NewReader(b))

		err = decoder.Decode(&snap)
		if err != nil {
			panic("Cannot parse input file:" + err.Error())
		}
		fmap.AddFeatureList(&snap.Features)
	}
	ofmx.UpdateReferences(fmap)
	//fmt.Println(len(fmap))

	airports := make(map[string]Airport)

	for _, f := range snap.Features.Slice() {
		switch t := f.(type) {
		case *types.Ahp:
			ap, ok := airports[t.AhpUid.String()]
			if !ok {
				ap = Airport{}
			}
			ap.ahp = *t
			airports[t.AhpUid.String()] = ap
		case *types.Ful:
			ap, ok := airports[t.FulUid.AhpUid.String()]
			if !ok {
				ap = Airport{}
			}
			ap.fuel = append(ap.fuel, *t)
			airports[t.FulUid.AhpUid.String()] = ap
		default:
			continue
		}
	}

	gjAhp := geojson.NewFeatureCollection()
	for _, ap := range airports {
		ahp := ap.ahp
		gf := &ap.ahp

		j, err := gf.GeoJson(fmap)
		if err != nil {
			fmt.Printf("Error, skipping: %s, %s\n", ahp.Uid().String(), err)
			continue
		}
		err = types.FillProperties(gf, j)
		if err != nil {
			fmt.Printf("Error, skipping: %s, %s\n", ahp.Uid().String(), err)
			continue
		}
		for _, f := range ap.fuel {
			flatten.FlattenPrefixedToResult(f, f.FulUid.CodeCat+"_", j.Properties)
		}
		gjAhp.AddFeature(j)
	}

	js, _ := gjAhp.MarshalJSON()
	os.WriteFile("fuelmap.geojson", js, 0644)
}
