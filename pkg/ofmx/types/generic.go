package types

import (
	"crypto/md5"
	"fmt"
	"io"
	"reflect"
	"strings"

	"gitlab.com/openflightmaps/go-ofmx/internal"
)

type MidMode = int

const (
	MID_MODE_PRESERVE MidMode = 0
	MID_MODE_CLIENT   MidMode = 1
	MID_MODE_OFMX     MidMode = 2
)

// TODO, those will be moved into a parser struct
//
//nolint:gochecknoglobals
var DefaultMidMode = MID_MODE_CLIENT

//nolint:gochecknoglobals
var IgnoreUndocumented = false

type Source struct {
	Source string `xml:"source,attr,omitempty" json:"source,omitempty" hash:"ignore"`
}

type Uid struct {
	Mid   string `xml:"mid,attr,omitempty" json:"mid,omitempty" hash:"ignore"`
	DBUid string `xml:"dbUid,attr,omitempty" hash:"ignore" json:"-"`

	// reference to object itself, skip validation to avoid recursion
	IRef interface{} `hash:"ignore" validate:"-" xml:"-" json:"-"`
}

type RegionalUid struct {
	Uid
	Region string `xml:"region,attr,omitempty" hash:"required" json:"region"`
}

type FeatureUid interface {
	String() string
	Hash() string
	OriginalMid() string
}

type Feature interface {
	Uid() FeatureUid
}

func (uid *Uid) OriginalMid() string {
	return uid.Mid
}

func UidString(uid interface{}) string {
	val := internal.IntHash(reflect.ValueOf(uid), false, DefaultMidMode == MID_MODE_OFMX)
	return fmt.Sprintf("%s|%s", reflect.ValueOf(uid).Type().Name(), strings.Join(val, "|"))
}

func UidHash(uid interface{}) string {
	val := internal.IntHash(reflect.ValueOf(uid), false, DefaultMidMode == MID_MODE_OFMX)
	s := fmt.Sprintf("%s|%s", reflect.ValueOf(uid).Type().Name(), strings.Join(val, "|"))
	h := md5.New()
	if _, err := io.WriteString(h, s); err != nil {
		panic(err)
	}
	m := fmt.Sprintf("%x", h.Sum(nil))

	return fmt.Sprintf("%s-%s-%s-%s-%s", m[:8], m[8:12], m[12:16], m[16:20], m[20:32])
}

func FeatureString(f Feature) string {
	v := reflect.ValueOf(f)
	val := []string{v.Type().Elem().Name()}
	val = append(val, internal.IntHash(v, false, DefaultMidMode == MID_MODE_OFMX)...)
	return strings.Join(val, "|")
}
