package types

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFeatureMapAdd(t *testing.T) {
	uid := AhpUid{RegionalUid: RegionalUid{Region: "LF"}, CodeId: "LFMD"}
	ahp := Ahp{
		AhpUid: uid,
	}

	fmap := NewFeatureMap()

	err := fmap.AddFeatures(&ahp)
	assert.Nil(t, err, "unable to add to featuremap")

	// TODO: adding again, here we should return a duplicate feature error
	err = fmap.AddFeatures(&ahp)
	assert.Nil(t, err, "unable to add to featuremap")

	f, err := fmap.ByUid(&uid)
	assert.Nil(t, err, "unable to find in featuremap")

	assert.Equal(t, "AhpUid|LF|LFMD", f.Uid().String())
	assert.Equal(t, "4b601836-9131-2bf6-6bd6-8cf5c48d367d", f.Uid().Hash())
}
