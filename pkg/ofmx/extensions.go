package ofmx

import "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"

// <OFM-AixmPlugin type="airspaceShapeExtension" xsi="http://www.aixm.aero/schema/4.5/OFMX-Snapshot.xsd" version="0.1" effective="2022-04-28T00:38:45" origin="ofmx editor" created="2022-04-28T00:39:21">

type OfmAixmPlugin struct {
	XMLName                   string            `xml:"OFM-AixmPlugin"`
	Xsi                       string            `xml:"xsi,attr"`
	NoNamespaceSchemaLocation string            `xml:"noNamespaceSchemaLocation,attr"`
	Version                   string            `xml:"version,attr"`
	Origin                    string            `xml:"origin,attr"`
	Namespace                 string            `xml:"namespace,attr"`
	Created                   string            `xml:"created,attr"`
	Effective                 string            `xml:"effective,attr"`
	Features                  types.FeatureList `xml:",any"`

	// decoder state and config
	config *decoderConfig `xml:"-" validate:"-"`
}

func (snap *OfmAixmPlugin) setDecoderConfig(c *decoderConfig) {
	snap.config = c
}
