package validation

import (
	"reflect"
	"strconv"
	"strings"

	ut "github.com/go-playground/universal-translator"
	"gopkg.in/go-playground/validator.v9"
)

func ValidateLat(fl validator.FieldLevel) bool {
	// 45.96933300N
	v := fl.Field().String()
	if v == "" {
		return false
	}
	ns := v[len(v)-1:]
	vn := v[0 : len(v)-1]
	if ns != "N" && ns != "S" {
		return false
	}
	f, err := strconv.ParseFloat(vn, 64)
	if err != nil {
		return false
	}
	s := strconv.FormatFloat(f, 'f', 8, 64)
	for len(s) < 11 {
		s = "0" + s
	}
	s = s + ns
	return s == v
}

func ValidateLong(fl validator.FieldLevel) bool {
	// 016.11361111E
	v := fl.Field().String()
	if v == "" {
		return false
	}
	ew := v[len(v)-1:]
	vn := v[0 : len(v)-1]
	if ew != "E" && ew != "W" {
		return false
	}
	f, err := strconv.ParseFloat(vn, 64)
	if err != nil {
		return false
	}
	s := strconv.FormatFloat(f, 'f', 8, 64)
	for len(s) < 12 {
		s = "0" + s
	}
	s = s + ew
	return s == v
}

func validateDmeChan(fl validator.FieldLevel) bool {
	// 1X-126X / 1Y-126Y
	v := fl.Field().String()
	if len(v) == 0 {
		return false
	}
	xy := v[len(v)-1:]
	vn := v[0 : len(v)-1]
	if xy != "X" && xy != "Y" {
		return false
	}
	n, err := strconv.Atoi(vn)
	if err != nil {
		return false
	}
	s := strconv.Itoa(n) + xy
	return s == v
}

func RegisterValidators(v *validator.Validate, trans ut.Translator) error {
	if err := v.RegisterValidation("lat", ValidateLat); err != nil {
		return err
	}
	if err := v.RegisterValidation("lon", ValidateLong); err != nil {
		return err
	}
	if err := v.RegisterValidation("dmeChan", validateDmeChan); err != nil {
		return err
	}

	v.RegisterAlias("wge", "eq=WGE")
	v.RegisterAlias("codeId", "required")
	v.RegisterAlias("textDesig", "required")
	// v.RegisterAlias("codeId", "alphanum") // TODO, enable
	v.RegisterAlias("yesno", "oneof=Y N")
	v.RegisterAlias("uomElev", "oneof=FT M FL")
	v.RegisterAlias("uomDist", "oneof=FT M")
	v.RegisterAlias("uomFreq", "oneof=MHZ KHZ")
	v.RegisterAlias("freq", "numeric")
	v.RegisterAlias("elev", "numeric")
	v.RegisterAlias("year", "numeric")
	v.RegisterAlias("bug", "isdefault")
	v.RegisterAlias("undocumented", "isdefault")
	v.RegisterAlias("date", "omitempty")
	v.RegisterAlias("time", "omitempty")

	v.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("xml"), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	err := v.RegisterTranslation("bug", trans, func(ut ut.Translator) error {
		return ut.Add("bug", "{0} should not exist according to OFMX specification! Please file a bug report", true) // see universal-translator for details
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T("bug", fe.Field())

		return t
	})
	return err
}
