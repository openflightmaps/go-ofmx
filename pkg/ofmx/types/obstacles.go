package types

// Obstacle Group

type OgrUid struct {
	RegionalUid

	TxtName string `xml:"txtName" validate:"required" json:"txtName"`
	LatLong
}

func (uid *OgrUid) String() string {
	return UidString(*uid)
}

func (uid *OgrUid) Hash() string {
	return UidHash(*uid)
}

type Ogr struct {
	Source
	OgrUid          OgrUid `xml:"OgrUid" validate:"required,dive" json:"OgrUid"`
	CodeDatum       string `xml:"codeDatum" validate:"required,oneof=WGE" json:"codeDatum"`
	ValGeoAccuracy  string `xml:"valGeoAccuracy,omitempty" validate:"omitempty,numeric" json:"valGeoAccuracy,omitempty"`
	UomGeoAccuracy  string `xml:"uomGeoAccuracy,omitempty" validate:"omitempty,oneof=FT KM M NM" json:"uomGeoAccuracy,omitempty"`
	ValElevAccuracy string `xml:"valElevAccuracy,omitempty" validate:"omitempty,numeric" json:"valElevAccuracy,omitempty"`
	UomElevAccuracy string `xml:"uomElevAccuracy,omitempty" validate:"omitempty,uomElev" json:"uomElevAccuracy,omitempty"`
	TxtRmk          string `xml:"txtRmk,omitempty" validate:"omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Ogr) Uid() FeatureUid {
	return &f.OgrUid
}

// Obstacle

type ObsUid struct {
	Uid
	OgrUid  OgrUid `xml:"OgrUid" validate:"required" json:"OgrUid"`
	GeoLat  string `xml:"geoLat" validate:"lat" json:"geoLat"`
	GeoLong string `xml:"geoLong" validate:"lon" json:"geoLong"`
}

func (uid *ObsUid) String() string {
	return UidString(*uid)
}

func (uid *ObsUid) Hash() string {
	return UidHash(*uid)
}

type Obs struct {
	Source
	ObsUid           ObsUid  `xml:"ObsUid" validate:"required,dive" json:"ObsUid"`
	TxtName          string  `xml:"txtName,omitempty" validate:"omitempty" json:"txtName,omitempty"`
	CodeType         string  `xml:"codeType,omitempty" validate:"required,oneof=ANTENNA BUILDING CHIMNEY CRANE MAST TOWER TREE WINDTURBINE OTHER" json:"codeType,omitempty"`
	CodeGroup        string  `xml:"codeGroup,omitempty" validate:"omitempty,oneof=Y N" json:"codeGroup,omitempty"`
	CodeLgt          string  `xml:"codeLgt,omitempty" validate:"omitempty,oneof=Y N" json:"codeLgt,omitempty"`
	CodeMarking      string  `xml:"codeMarking,omitempty" validate:"omitempty,oneof=Y N" json:"codeMarking,omitempty"`
	TxtDescrLgt      string  `xml:"txtDescrLgt,omitempty" validate:"omitempty" json:"txtDescrLgt,omitempty"`
	TxtDescrMarking  string  `xml:"txtDescrMarking,omitempty" validate:"omitempty" json:"txtDescrMarking,omitempty"`
	CodeDatum        string  `xml:"codeDatum" validate:"required,oneof=WGE" json:"codeDatum"`
	ValElev          string  `xml:"valElev" validate:"required,numeric" json:"valElev"`
	ValHgt           string  `xml:"valHgt,omitempty" validate:"omitempty,numeric" json:"valHgt,omitempty"`
	UomDistVer       string  `xml:"uomDistVer" validate:"required,oneof=FT M" json:"uomDistVer"`
	CodeHgtAccuracy  string  `xml:"codeHgtAccuracy,omitempty" validate:"omitempty,oneof=Y N" json:"codeHgtAccuracy,omitempty"`
	ValRadius        string  `xml:"valRadius,omitempty" validate:"omitempty,numeric" json:"valRadius,omitempty"`
	UomRadius        string  `xml:"uomRadius,omitempty" validate:"omitempty,oneof=FT KM M NM" json:"uomRadius,omitempty"`
	ObsUidLink       *ObsUid `xml:"ObsUidLink,omitempty" validate:"omitempty,dive" json:"ObsUidLink,omitempty"`
	CodeLinkType     string  `xml:"codeLinkType,omitempty" validate:"omitempty,oneof=CABLE SOLID OTHER" json:"codeLinkType,omitempty"`
	DatetimeValidWef string  `xml:"datetimeValidWef,omitempty" validate:"omitempty,oneof=datetime" json:"datetimeValidWef,omitempty"`
	DatetimeValidTil string  `xml:"datetimeValidTil,omitempty" validate:"omitempty,oneof=datetime" json:"datetimeValidTil,omitempty"`
	TxtRmk           string  `xml:"txtRmk,omitempty" validate:"omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Obs) Uid() FeatureUid {
	return &f.ObsUid
}

// Rdo - Runway Direction Obstacle

type RdoUid struct {
	Uid
	ObsUid *ObsUid `xml:"ObsUid" json:"ObsUid"`
	RdnUid *RdnUid `xml:"RdnUid" json:"RdnUid"`
}

func (uid *RdoUid) String() string {
	return UidString(*uid)
}

func (uid *RdoUid) Hash() string {
	return UidHash(*uid)
}

type Rdo struct {
	RdoUid      RdoUid `xml:"RdoUid" json:"RdoUid"`
	CodeTypeOps string `xml:"codeTypeOps,omitempty" json:"codeTypeOps,omitempty"`
	TxtRmk      string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Rdo) Uid() FeatureUid {
	return &f.RdoUid
}

// Fdo - FATO Direction Obstacle

type FdoUid struct {
	Uid
	ObsUid *ObsUid `xml:"ObsUid" json:"ObsUid"`
	FdnUid *FdnUid `xml:"FdnUid" json:"FdnUid"`
	validation
}

func (uid *FdoUid) String() string {
	return UidString(*uid)
}

func (uid *FdoUid) Hash() string {
	return UidHash(*uid)
}

type Fdo struct {
	FdoUid      FdoUid `xml:"FdoUid" json:"FdoUid"`
	CodeTypeOps string `xml:"codeTypeOps,omitempty" json:"codeTypeOps,omitempty"`
	TxtRmk      string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Fdo) Uid() FeatureUid {
	return &f.FdoUid
}
