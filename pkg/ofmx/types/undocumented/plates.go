package undocumented

import ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"

// TODO: document those

type PpaUid struct {
	ofmx.RegionalUid
	PackageType            string `xml:"packageType" json:"packageType"`
	EntityOfInterestCodeId string `xml:"entityOfInterestCodeId" json:"entityOfInterestCodeId"`
	validation
}

func (uid *PpaUid) String() string {
	return ofmx.UidString(*uid)
}

func (uid *PpaUid) Hash() string {
	return ofmx.UidHash(*uid)
}

type Ppa struct {
	PpaUid PpaUid `xml:"PpaUid" json:"PpaUid"`

	TxtPurpose        string       `xml:"txtPurpose,omitempty" json:"txtPurpose,omitempty"`
	TxtPurposeLocLang string       `xml:"txtPurposeLocLang,omitempty" json:"txtPurposeLocLang,omitempty"`
	StatusRelease     string       `xml:"statusRelease,omitempty" json:"statusRelease,omitempty"`
	DisplayType       string       `xml:"displayType,omitempty" validate:"undocumented" json:"displayType,omitempty"`
	XtImageGroup      XtImageGroup `xml:"xt_imageGroup,omitempty" validate:"undocumented" json:"xt_imageGroup,omitempty"`
	validation
}

func (f *Ppa) Uid() ofmx.FeatureUid {
	return &f.PpaUid
}

type PlpUid struct {
	ofmx.Uid
	PpaUid          PpaUid `xml:"PpaUid" json:"PpaUid"`
	PlateIdentifier string `xml:"plateIdentifier" json:"plateIdentifier"`
	validation
}

func (uid *PlpUid) String() string {
	return ofmx.UidString(*uid)
}

func (uid *PlpUid) Hash() string {
	return ofmx.UidHash(*uid)
}

type XtImageGroup struct {
	Img string `xml:"img" json:"img"`
}

type BrStrip struct {
	Type            string `xml:"type" json:"type" validate:"oneof=AD-DATA-STRIP FATO-NOTES FREQ-STRIP TLOF-NOTES FATO-DIR-NOTES HEADER-TEXT RWY-STRIP LANG-NOTE-STRIP OPS-HRS-STRIP BLACKBOX-NOTE-1 BLACKBOX-NOTE-2 BLACKBOX-NOTE-3 REDBOX-NOTE-1 REDBOX-NOTE-2 REDBOX-NOTE-3 LANG-NOTE-FULL LANG-NOTE-FULL-WIDTH AIRAC-NOTE AIRAC-INFO SCALE-RULER AD-LIST-FIR CONTROLLED-AIRSPACE-INFO-FIR LANG-NOTE-STRIP-BOX PRC-ADMIN-STRIP QR-STRIP AD-QR-STRIP PP-QR-STRIP TYPE-OF-AC-STRIP FATO-LIGHT-NOTES AD-RMK-STRIP AD-RMK-STRIP-FULL IMG-STRIP RMK-STRIP FUEL-STRIP NOTE CHART-HEADER AD-LIST-ALL APP-FREQ FIC-FREQ RAD-FREQ CONTROLLED-AIRSPACE-INFO-ALL GRIDSCALE-STRIP LEGEND-SHORT MAP-INFO LEGEND AD-LIST"`
	BoxWidth        string `xml:"boxWidth" json:"boxWidth"`
	BoxHeight       string `xml:"boxHeight" json:"boxHeight"`
	TxtValue        string `xml:"txtValue" json:"txtValue"`
	TxtValueLocLang string `xml:"txtValueLocLang" json:"txtValueLocLang"`
	Orientation     string `xml:"orientation,omitempty" json:"orientation,omitempty"`
	PointerGeoLat   string `xml:"pointerGeoLat,omitempty" json:"pointerGeoLat,omitempty"`
	PointerGeoLong  string `xml:"pointerGeoLong,omitempty" json:"pointerGeoLong,omitempty"`
	GmlPosList      string `xml:"gmlPosList" json:"gmlPosList"`
	Value           string `xml:"value,omitempty" json:"value,omitempty"`
	validation
}

type FrameShot struct {
	GmlPosList string `xml:"gmlPosList" json:"gmlPosList"`
}

type Plp struct {
	PlpUid                  PlpUid    `xml:"PlpUid" json:"PlpUid"`
	TxtDesig                string    `xml:"txtDesig" json:"txtDesig"`
	CodeType                string    `xml:"codeType" json:"codeType"`
	Cat                     string    `xml:"cat" json:"cat"`
	XXXzoomLevel            string    `xml:"zoomLevel" validate:"undocumented" json:"zoomLevel"`
	VisScale                string    `xml:"visScale,omitempty" json:"visScale,omitempty"`
	AspectRatio             string    `xml:"aspectRatio,omitempty" json:"aspectRatio,omitempty"`
	MaxPageHeightMM         string    `xml:"maxPageHeight_mm,omitempty" json:"maxPageHeight_mm,omitempty"`
	MaxPageWidthMM          string    `xml:"maxPageWidth_mm,omitempty" json:"maxPageWidth_mm,omitempty"`
	NbrFoldingMarksX        string    `xml:"NbrFoldingMarksX,omitempty" json:"NbrFoldingMarksX,omitempty"`
	NbrFoldingMarksY        string    `xml:"NbrFoldingMarksY,omitempty" json:"NbrFoldingMarksY,omitempty"`
	Frame                   string    `xml:"frame" json:"frame"`
	XXXbriefingStripPattern string    `xml:"briefingStripPattern" validate:"undocumented" json:"briefingStripPattern"`
	TxtDesignSet            string    `xml:"txtDesignSet" json:"txtDesignSet"`
	BasemapTransparency     string    `xml:"basemapTransparency" json:"basemapTransparency"`
	StaticLabelSource       string    `xml:"staticLabelSource" json:"staticLabelSource"`
	ForcedBasemapLevel      string    `xml:"forcedBasemapLevel,omitempty" json:"forcedBasemapLevel,omitempty"`
	BasemapTileURL          string    `xml:"basemapTileURL,omitempty" json:"basemapTileURL,omitempty"`
	VisSectionalLabels      string    `xml:"VisSectionalLabels" json:"VisSectionalLabels"`
	Projection              string    `xml:"projection" json:"projection"`
	StandardParallel1       string    `xml:"standardParallel1,omitempty" json:"standardParallel1,omitempty"`
	StandardParallel2       string    `xml:"standardParallel2,omitempty" json:"standardParallel2,omitempty"`
	FrameShot               FrameShot `xml:"frameShot" json:"frameShot"`
	VisGridMora             string    `xml:"visGridMora,omitempty" json:"visGridMora,omitempty"`
	MoraNbrOfSecPerDegrLat  string    `xml:"moraNbrOfSecPerDegrLat,omitempty" json:"moraNbrOfSecPerDegrLat,omitempty"`
	MoraNbrOfSecPerDegrLon  string    `xml:"moraNbrOfSecPerDegrLon,omitempty" json:"moraNbrOfSecPerDegrLon,omitempty"`
	BrStrip                 []BrStrip `xml:"brStrip" json:"brStrip" validate:"dive"`

	// TODO, document ??
	XXXBasemapOpacity string `xml:"basemapOpacity,omitempty" validate:"isdefault" json:"basemapOpacity,omitempty"`
	XXXTerrainOpacity string `xml:"terrainOpacity,omitempty" validate:"isdefault" json:"terrainOpacity,omitempty"`
	validation
}

func (f *Plp) Uid() ofmx.FeatureUid {
	return &f.PlpUid
}
