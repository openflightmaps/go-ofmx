package ofmx

import (
	"encoding/xml"
	"errors"
	"io"
	"log"
	"reflect"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types/undocumented"
	"golang.org/x/net/html/charset"
)

func newUndocumentedFeature(name string) (types.Feature, error) {
	switch name {
	case "Twy":
		return &undocumented.Twy{}, nil
	case "Tly":
		return &undocumented.Tly{}, nil
	case "Apn":
		return &undocumented.Apn{}, nil
	// TODO, handle undocumented features
	// OFM Plate Archive?
	case "Ppa":
		// TODO Ppa
		return &undocumented.Ppa{}, nil
		// OFM Platepackage
	case "Plp":
		// TODO Plp
		return &undocumented.Plp{}, nil
		// OFM Procedure
	case "Prc":
		// TODO Prc
		return &undocumented.Prc{}, nil
		// Misc structure
	case "Msc":
		// TODO Msc
		return &undocumented.Msc{}, nil
	}
	return nil, errors.New("unknown type " + name)
}

func NewFeature(name string, skipUndocumented bool) (types.Feature, error) {
	// Org/Frequency
	switch name {
	case "Org":
		return &types.Org{}, nil
	case "Uni":
		return &types.Uni{}, nil
	case "Ser":
		return &types.Ser{}, nil
	case "Fqy":
		return &types.Fqy{}, nil

	// Airport
	case "Ahp":
		return &types.Ahp{}, nil
	case "Ful":
		return &types.Ful{}, nil
	case "Rwy":
		return &types.Rwy{}, nil
	case "Rdn":
		return &types.Rdn{}, nil
	case "Rdd":
		return &types.Rdd{}, nil
	case "Rls":
		return &types.Rls{}, nil
	case "Ahu":
		return &types.Ahu{}, nil
	case "Ahs":
		return &types.Ahs{}, nil
	case "Aga":
		return &types.Aga{}, nil
	case "Aha":
		return &types.Aha{}, nil
	case "Rda":
		return &types.Rda{}, nil
	case "Sah":
		return &types.Sah{}, nil

	// Heliports
	case "Fto":
		return &types.Fto{}, nil
	case "Fdn":
		return &types.Fdn{}, nil
	case "Fdd":
		return &types.Fdd{}, nil
	case "Tla":
		return &types.Tla{}, nil
	case "Tls":
		return &types.Tls{}, nil
	case "Fda":
		return &types.Fda{}, nil
	case "Fls":
		return &types.Fls{}, nil

	// Navigation
	case "Dpn":
		return &types.Dpn{}, nil
	case "Dme":
		return &types.Dme{}, nil
	case "Mkr":
		return &types.Mkr{}, nil
	case "Ndb":
		return &types.Ndb{}, nil
	case "Tcn":
		return &types.Tcn{}, nil
	case "Vor":
		return &types.Vor{}, nil

	// Navigation Aids
	case "Ils":
		return &types.Ils{}, nil

		// airspace
	case "Ase":
		return &types.Ase{}, nil
	case "Abd":
		return &types.Abd{}, nil
	// missing Avx
	case "Adg":
		return &types.Adg{}, nil
	case "Sae":
		return &types.Sae{}, nil

	// Geographical border
	case "Gbr":
		return &types.Gbr{}, nil
	// Geographical border vertex // not a standalone feature
	case "Gbv":

	// Obstacle Group
	case "Ogr":
		return &types.Ogr{}, nil

	// Obstacle
	case "Obs":
		return &types.Obs{}, nil

	// Runway direction obstacle
	case "Rdo":
		return &types.Rdo{}, nil

	// FATO direction obstacle
	case "Fdo":
		return &types.Fdo{}, nil

	// OFM Label Marker
	case "Lbm":
		return &types.Lbm{}, nil

	default:
		undoc, err := newUndocumentedFeature(name)
		if err != nil {
			return undoc, err
		}
		if !skipUndocumented {
			return undoc, err
		}
	}

	return nil, errors.New("unknown type " + name)
}

func UpdateReferences(fmap types.FeatureMap) {
	for _, f := range fmap.Slice() {
		// set uid self-reference
		reflect.ValueOf(f.Uid()).Elem().FieldByName("IRef").Set(reflect.ValueOf(f))

		// update direct references
		s := reflect.ValueOf(f).Elem()
		for i := 0; i < s.NumField(); i++ {
			fld := s.Field(i)

			if fld.Kind() == reflect.Ptr {
				if fld.IsNil() {
					continue
				}
				if uid, ok := fld.Interface().(types.FeatureUid); ok {
					// replace with reference
					if ref, err := fmap.ByUid(uid); err == nil {
						fld.Set(reflect.ValueOf(ref.Uid()))
					} else {
						log.Printf("reference not found: %s => %s", f.Uid().String(), uid.String())
					}
				}
			}
		}

		// update references within uid
		s = reflect.ValueOf(f.Uid()).Elem()
		for i := 0; i < s.NumField(); i++ {
			fld := s.Field(i)

			if fld.Kind() == reflect.Ptr {
				if fld.IsNil() {
					continue
				}
				if uid, ok := fld.Interface().(types.FeatureUid); ok {
					// replace with reference
					if ref, err := fmap.ByUid(uid); err == nil {
						fld.Set(reflect.ValueOf(ref.Uid()))
					} else {
						log.Printf("reference not found: %s => %s", f.Uid().String(), uid.String())
					}
				}
			}
		}

		// loop through types
		switch v := f.(type) {
		case *types.Ahu:
			// backlink
			if a, err := fmap.ByUid(v.AhuUid.AhpUid); err == nil {
				ahp := a.(*types.Ahp)
				if ahp.Ahu != nil {
					log.Println("duplicate Ahu for airport ", a.Uid().String())
				}
				ahp.Ahu = v
			}
		case *types.Adg:
			// backlink
			if a, err := fmap.ByUid(v.AdgUid.AseUid); err == nil {
				ase := a.(*types.Ase)
				ase.Adg = v
			}
		case *types.Abd:
			// backlink
			if a, err := fmap.ByUid(v.AbdUid.AseUid); err == nil {
				ase := a.(*types.Ase)
				ase.Abd = v
			}
		case *types.Ful:
			// backlink
			if a, err := fmap.ByUid(v.FulUid.AhpUid); err == nil {
				ahp := a.(*types.Ahp)
				ahp.Ful = append(ahp.Ful, v)
			}
		case *types.Aha:
			// backlink
			if a, err := fmap.ByUid(v.AhaUid.AhpUid); err == nil {
				ahp := a.(*types.Ahp)
				ahp.Aha = append(ahp.Aha, v)
			}
		case *types.Aga:
			// backlink
			if a, err := fmap.ByUid(v.AgaUid.AhsUid); err == nil {
				ahs := a.(*types.Ahs)
				ahs.Aga = append(ahs.Aga, v)
			}
		case *types.Ahs:
			// backlink
			if a, err := fmap.ByUid(v.AhsUid.AhpUid); err == nil {
				ahp := a.(*types.Ahp)
				ahp.Ahs = append(ahp.Ahs, v)
			}
		case *types.Ahp:
			// forward-link
			if a, err := fmap.ByUid(&v.OrgUid); err == nil {
				f := a.(*types.Org)
				v.Org = f
			}
		case *types.Sae:
			if a, err := fmap.ByUid(v.SaeUid.AseUid); err == nil {
				ase := a.(*types.Ase)
				if b, err := fmap.ByUid(v.SaeUid.SerUid); err == nil {
					ser := b.(*types.Ser)
					ase.Ser = ser
				}
			}
		}
	}
}

type decoderConfig struct {
	IgnoreUndocumented bool
	MidMode            types.MidMode
}

type Decoder struct {
	decoder *xml.Decoder
	config  *decoderConfig
}

type OfmxDecodable interface {
	setDecoderConfig(*decoderConfig)
}

type DecoderOption func(*decoderConfig)

func NewDecoder(r io.Reader, options ...DecoderOption) *Decoder {
	decoder := Decoder{
		config: &decoderConfig{
			IgnoreUndocumented: true,
			MidMode:            types.MID_MODE_OFMX,
		},
	}
	// apply config
	for _, opt := range options {
		opt(decoder.config)
	}
	decoder.decoder = xml.NewDecoder(r)
	decoder.decoder.Strict = true
	decoder.decoder.CharsetReader = charset.NewReaderLabel
	return &decoder
}

func (d *Decoder) Decode(v OfmxDecodable) error {
	v.setDecoderConfig(d.config)
	err := d.decoder.Decode(v)
	return err
}

func AllowUndocumented() DecoderOption {
	return func(c *decoderConfig) {
		c.IgnoreUndocumented = false
	}
}

// Deprecated: use AllowUndocumented
func IgnoreUndocumented(b bool) DecoderOption {
	return func(c *decoderConfig) {
		c.IgnoreUndocumented = b
	}
}

func MidMode(m types.MidMode) DecoderOption {
	return func(c *decoderConfig) {
		c.MidMode = m
	}
}

type featureList struct {
	features []types.Feature
}

func NewFeatureList() types.FeatureList {
	return &featureList{}
}

func (s *featureList) Append(features ...types.Feature) types.FeatureList {
	s.features = append(s.features, features...)
	return s
}

func (s featureList) Slice() []types.Feature {
	return s.features
}

func (s featureList) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	for _, f := range s.features {
		start.Name.Local = reflect.TypeOf(f).Elem().Name()
		if err := e.EncodeElement(f, start); err != nil {
			return err
		}
	}

	return nil
}

func (s *featureList) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	e := ParseError{}
	feature, err := NewFeature(start.Name.Local, types.IgnoreUndocumented)
	if err == nil {
		if err := d.DecodeElement(&feature, &start); err != nil {
			log.Printf("Error in %s: %v\n", feature.Uid().String(), err)
			return err
		}
		s.features = append(s.features, feature)
	} else {
		e.Errors = append(e.Errors, err)
		log.Println("Unknown feature: " + start.Name.Local)
		if err = d.Skip(); err != nil {
			log.Printf("Error in %s: %v\n", feature.Uid().String(), err)
			return err
		}
	}
	// TODO: Unimplemented
	/*
		if len(e.Errors) > 0 {
			e.Message = "Errors occurred during parsing"
			return e
		}
	*/
	return nil
}
