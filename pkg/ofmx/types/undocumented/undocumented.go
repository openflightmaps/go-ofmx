package undocumented

import "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"

// Twy - Invalid/Deprecated, Ex Taxiway

type TwyUid struct {
	// TODO, temp allow mid
	types.Uid

	AhpUid   *types.AhpUid `xml:"AhpUid" json:"AhpUid"`
	TxtDesig string        `xml:"txtDesig" json:"txtDesig"`
}

func (uid *TwyUid) String() string {
	return types.UidString(*uid)
}

func (uid *TwyUid) Hash() string {
	return types.UidHash(*uid)
}

type Twy struct {
	TwyUid   TwyUid `xml:"TwyUid" validate:"dive" json:"TwyUid"`
	CodeType string `xml:"codeType,omitempty" json:"codeType,omitempty"`
	ValWid   string `xml:"valWid,omitempty" json:"valWid,omitempty"`
	UomWid   string `xml:"uomWid,omitempty" json:"uomWid,omitempty"`
	types.SurfaceCharacteristics
	CodeStrength     string    `xml:"codeStrength,omitempty" json:"codeStrength,omitempty"`         // DEPRECATED
	TxtDescrStrength string    `xml:"txtDescrStrength,omitempty" json:"txtDescrStrength,omitempty"` // DEPRECATED
	CodeSts          string    `xml:"codeSts,omitempty" json:"codeSts,omitempty"`
	TxtMarking       string    `xml:"txtMarking,omitempty" json:"txtMarking,omitempty"`
	TxtRmk           string    `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	XtSurface        XtSurface `xml:"xt_surface,omitempty" validate:"dive" json:"xt_surface,omitempty"`

	// TODO document
	XtLabel []XtLabel `xml:"xt_label" validate:"dive" json:"xt_label"`
}

func (f *Twy) Uid() types.FeatureUid {
	return &f.TwyUid
}

// Tly - Taxiway Lighting

type TlyUid struct {
	types.Uid

	TwyUid  *TwyUid `xml:"TwyUid" json:"TwyUid"`
	CodePsn string  `xml:"codePsn" json:"codePsn"`
}

func (uid *TlyUid) String() string {
	return types.UidString(*uid)
}

func (uid *TlyUid) Hash() string {
	return types.UidHash(*uid)
}

type Tly struct {
	TlyUid    TlyUid     `xml:"TlyUid" validate:"dive" json:"TlyUid"`
	TxtRmk    string     `xml:"txtRmk" json:"txtRmk"`
	XtSurface *XtSurface `xml:"xt_surface,omitempty" validate:"omitempty,dive" json:"xt_surface"`
}

func (f *Tly) Uid() types.FeatureUid {
	return &f.TlyUid
}

// Msc - Miscellaneous

type MscUid struct {
	types.Uid

	AhpUid  *types.AhpUid `xml:"AhpUid" validate:"dive" json:"AhpUid"`
	TxtName string        `xml:"txtName" json:"txtName"`
}

func (uid *MscUid) String() string {
	return types.UidString(*uid)
}

func (uid *MscUid) Hash() string {
	return types.UidHash(*uid)
}

type XtSurface struct {
	GmlPosList string `xml:"gmlPosList" json:"gmlPosList"`
}

type XtLabel struct {
	VisThrM string `xml:"visThrM" json:"visThrM"`
	Angle   string `xml:"angle" json:"angle"`
	GeoLat  string `xml:"geoLat" json:"geoLat"`
	GeoLong string `xml:"geoLong" json:"geoLong"`
}

// TODO, document
type Msc struct {
	MscUid MscUid `xml:"MscUid" json:"MscUid"`
	Type   string `xml:"type" json:"type"`
	TxtRmk string `xml:"txtRmk" json:"txtRmk"`

	// TODO document
	XtSurface XtSurface `xml:"xt_surface" validate:"undocumented" json:"xt_surface"`
	XtLabel   []XtLabel `xml:"xt_label" json:"xt_label"`
}

func (f *Msc) Uid() types.FeatureUid {
	return &f.MscUid
}
