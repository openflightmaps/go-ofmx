package types

type OrgUid struct {
	RegionalUid
	TxtName string `xml:"txtName" json:"txtName"`
}

func (uid *OrgUid) String() string {
	return UidString(*uid)
}

func (uid *OrgUid) Hash() string {
	return UidHash(*uid)
}

type Org struct {
	Source
	OrgUid   OrgUid
	CodeId   string `xml:"codeId,omitempty" json:"codeId,omitempty"`
	CodeType string `xml:"codeType" json:"codeType"`
	TxtRmk   string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Org) Uid() FeatureUid {
	return &f.OrgUid
}

type UniUid struct {
	RegionalUid

	TxtName  string `xml:"txtName" json:"txtName"`
	CodeType string `xml:"codeType" json:"codeType"`
}

func (uid *UniUid) String() string {
	return UidString(*uid)
}

func (uid *UniUid) Hash() string {
	return UidHash(*uid)
}

func (uid *UniUid) Ref() *Uni {
	if uid.IRef == nil {
		// TODO, needed? BUGON
		return nil
	}
	return uid.IRef.(*Uni)
}

type Uni struct {
	Source
	UniUid    UniUid  `xml:"UniUid" json:"UniUid" validate:"dive"`
	OrgUid    *OrgUid `xml:"OrgUid" json:"OrgUid" validate:"dive"`
	AhpUid    *AhpUid `xml:"AhpUid" json:"AhpUid" validate:"omitempty,dive"`
	CodeClass string  `xml:"codeClass" json:"codeClass" validate:"oneof=ICAO OTHER"`
	TxtRmk    string  `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Uni) Uid() FeatureUid {
	return &f.UniUid
}

type SerUid struct {
	// TODO, temp allow mid
	Uid

	UniUid   *UniUid `xml:"UniUid" json:"UniUid" validate:"dive"`
	CodeType string  `xml:"codeType" json:"codeType" validate:"oneof=ACS AFIS AIS APP ATIS COM CTAF FIS GCA NOF MET PAR RADAR SMC TWEB TWR VDF OTHER"`
	NoSeq    int     `xml:"noSeq" json:"noSeq" validate:"numeric"`
}

func (uid *SerUid) String() string {
	return UidString(*uid)
}

func (uid *SerUid) Hash() string {
	return UidHash(*uid)
}

func (uid *SerUid) Ref() *Ser {
	if uid.IRef == nil {
		// TODO, needed? BUGON
		return nil
	}
	return uid.IRef.(*Ser)
}

type Ser struct {
	Source
	SerUid SerUid      `xml:"SerUid" json:"SerUid" validate:"dive"`
	Stt    []Timetable `xml:"Stt" json:"Stt" validate:"omitempty,dive"`
	TxtRmk string      `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Ser) Uid() FeatureUid {
	return &f.SerUid
}

type FqyUid struct {
	// TODO, temp allow mid
	Uid

	SerUid       *SerUid `xml:"SerUid" json:"SerUid"`
	ValFreqTrans string  `xml:"valFreqTrans" json:"valFreqTrans"`
}

func (uid *FqyUid) String() string {
	return UidString(*uid)
}

func (uid *FqyUid) Hash() string {
	return UidHash(*uid)
}

type Cdl struct {
	TxtCallSign string `xml:"txtCallSign" json:"txtCallSign"`
	CodeLang    string `xml:"codeLang" json:"codeLang"`
	validation
}

type Fqy struct {
	Source
	FqyUid     FqyUid      `xml:"FqyUid" json:"FqyUid"`
	ValFreqRec string      `xml:"valFreqRec,omitempty" json:"valFreqRec,omitempty"`
	UomFreq    string      `xml:"uomFreq" json:"uomFreq"`
	CodeType   string      `xml:"codeType,omitempty" json:"codeType,omitempty"`
	Ftt        []Timetable `xml:"Ftt,omitempty" json:"Ftt,omitempty"`
	TxtRmk     string      `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	Cdl        []Cdl       `xml:"Cdl" json:"Cdl"`
	validation
}

func (f *Fqy) Uid() FeatureUid {
	return &f.FqyUid
}
