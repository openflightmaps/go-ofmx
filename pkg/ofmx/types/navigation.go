package types

type DpnUid struct {
	RegionalUid
	CodeId string `xml:"codeId" json:"codeId" validate:"codeId"`
	LatLong

	// TODO: check if correct
	validation
}

func (uid *DpnUid) String() string {
	return UidString(*uid)
}

func (uid *DpnUid) Hash() string {
	return UidHash(*uid)
}

type Dpn struct {
	Source
	DpnUid      DpnUid  `xml:"DpnUid" json:"DpnUid" validate:"dive"`
	AhpUidAssoc *AhpUid `xml:"AhpUidAssoc" json:"AhpUidAssoc" validate:"omitempty,dive"`
	CodeDatum   string  `xml:"codeDatum" json:"codeDatum" validate:"wge"`
	CodeType    string  `xml:"codeType" json:"codeType" validate:"oneof=ICAO ADHP COORD VFR-RP VFR-MRP VFR-ENR VFR-GLDR VFR-HELI TOWN MOUNTAIN-TOP MOUNTAIN-PASS OTHER"`
	TxtName     string  `xml:"txtName,omitempty" json:"txtName,omitempty"`
	ValElev     string  `xml:"valElev,omitempty" json:"valElev,omitempty" validate:"omitempty,numeric"`
	UomElev     string  `xml:"uomElev,omitempty" json:"uomElev,omitempty" validate:"omitempty,uomDist"`
	ValTrueBrg  string  `xml:"valTrueBrg,omitempty" json:"valTrueBrg,omitempty" validate:"omitempty,numeric,len=3"` // must be left-zero padded eg. 090
	ValMagBrg   string  `xml:"valMagBrg,omitempty" json:"valMagBrg,omitempty" validate:"omitempty,numeric,len=3"`   // must be left-zero padded eg. 090
	TxtRmk      string  `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`

	// // TODO, drop, invalid
	// XXvalCrs        string `xml:"valCrs,omitempty" json:"-" validate:"undocumented"`
	// XXvalHgt        string `xml:"valHgt,omitempty" json:"-" validate:"undocumented"`
	// XXuomDistVer    string `xml:"uomDistVer,omitempty" json:"-" validate:"undocumented"`
	// XXuomFreq       string `xml:"uomFreq,omitempty" json:"-" validate:"undocumented"`
	// XXcodeTypeNorth string `xml:"codeTypeNorth,omitempty" json:"-" validate:"undocumented"`
	validation
}

func (f *Dpn) Uid() FeatureUid {
	return &f.DpnUid
}

type DmeUid struct {
	RegionalUid
	CodeId string `xml:"codeId" json:"codeId" validate:"codeId"`
	LatLong

	// TODO: check if correct
	validation
}

func (uid *DmeUid) String() string {
	return UidString(*uid)
}

func (uid *DmeUid) Hash() string {
	return UidHash(*uid)
}

type Dme struct {
	Source
	DmeUid       DmeUid      `xml:"DmeUid" json:"DmeUid" validate:"dive"`
	OrgUid       *OrgUid     `xml:"OrgUid" json:"OrgUid"`
	VorUid       *VorUid     `xml:"VorUid" json:"VorUid"`
	TxtName      string      `xml:"txtName,omitempty" json:"txtName,omitempty"`
	CodeChannel  string      `xml:"codeChannel" json:"codeChannel" validate:"required,dmeChan"`
	ValGhostFreq string      `xml:"valGhostFreq,omitempty" json:"valGhostFreq,omitempty" validate:"omitempty,freq"`
	UomGhostFreq string      `xml:"uomGhostFreq,omitempty" json:"uomGhostFreq,omitempty" validate:"omitempty,uomFreq"`
	CodeDatum    string      `xml:"codeDatum" json:"codeDatum" validate:"wge"`
	ValElev      string      `xml:"valElev,omitempty" json:"valElev,omitempty" validate:"omitempty,elev"`
	UomDistVer   string      `xml:"uomDistVer,omitempty" json:"uomDistVer,omitempty" validate:"omitempty,uomDist"`
	Dtt          []Timetable `xml:"Dtt,omitempty" json:"Dtt,omitempty" validate:"dive"`
	TxtRmk       string      `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Dme) Uid() FeatureUid {
	return &f.DmeUid
}

type MkrUid struct {
	RegionalUid
	CodeId string `xml:"codeId" json:"codeId" validate:"codeId"`
	LatLong
}

func (uid *MkrUid) String() string {
	return UidString(*uid)
}

func (uid *MkrUid) Hash() string {
	return UidHash(*uid)
}

type Mkr struct {
	Source
	MkrUid     MkrUid      `xml:"MkrUid" json:"MkrUid" validate:"dive"`
	OrgUid     *OrgUid     `xml:"OrgUid" json:"OrgUid"`
	IlsUid     IlsUid      `xml:"IlsUid" json:"IlsUid" validate:"dive"`
	CodePsnIls string      `xml:"codePsnIls,omitempty" json:"codePsnIls,omitempty"`
	ValFreq    string      `xml:"valFreq" json:"valFreq" validate:"freq"`
	UomFreq    string      `xml:"uomFreq" json:"uomFreq" validate:"uomFreq"`
	TxtName    string      `xml:"txtName,omitempty" json:"txtName,omitempty"`
	CodeDatum  string      `xml:"codeDatum" json:"codeDatum" validate:"wge"`
	ValElev    string      `xml:"valElev,omitempty" json:"valElev,omitempty" validate:"omitempty,elev"`
	UomDistVer string      `xml:"uomDistVer,omitempty" json:"uomDistVer,omitempty" validate:"omitempty,uomDist"`
	Mtt        []Timetable `xml:"Mtt" json:"Mtt" validate:"dive"`
	TxtRmk     string      `xml:"txtRmk" json:"txtRmk"`
	validation
}

func (f *Mkr) Uid() FeatureUid {
	return &f.MkrUid
}

type NdbUid struct {
	RegionalUid
	CodeId string `xml:"codeId" json:"codeId" validate:"codeId"`
	LatLong

	// TODO: check if correct
	validation
}

func (uid *NdbUid) String() string {
	return UidString(*uid)
}

func (uid *NdbUid) Hash() string {
	return UidHash(*uid)
}

type Ndb struct {
	Source
	NdbUid     NdbUid      `xml:"NdbUid" validate:"dive" json:"NdbUid"`
	OrgUid     *OrgUid     `xml:"OrgUid" json:"OrgUid"`
	TxtName    string      `xml:"txtName,omitempty" json:"txtName,omitempty"`
	ValFreq    string      `xml:"valFreq" validate:"freq" json:"valFreq"`
	UomFreq    string      `xml:"uomFreq" validate:"eq=KHZ" json:"uomFreq"`
	CodeClass  string      `xml:"codeClass,omitempty" validate:"omitempty,oneof=B L M OTHER" json:"codeClass,omitempty"`
	CodeDatum  string      `xml:"codeDatum" validate:"wge" json:"codeDatum"`
	ValElev    string      `xml:"valElev,omitempty" validate:"omitempty,elev" json:"valElev,omitempty"`
	UomDistVer string      `xml:"uomDistVer,omitempty" validate:"omitempty,uomDist" json:"uomDistVer,omitempty"`
	Ntt        []Timetable `xml:"Ntt,omitempty" validate:"dive" json:"Ntt,omitempty"`
	TxtRmk     string      `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Ndb) Uid() FeatureUid {
	return &f.NdbUid
}

type TcnUid struct {
	RegionalUid
	CodeId string `xml:"codeId" json:"codeId" validate:"codeId"`
	LatLong
}

func (uid *TcnUid) String() string {
	return UidString(*uid)
}

func (uid *TcnUid) Hash() string {
	return UidHash(*uid)
}

type Tcn struct {
	Source
	TcnUid       TcnUid      `xml:"TcnUid" validate:"dive" json:"TcnUid"`
	OrgUid       *OrgUid     `xml:"OrgUid" json:"OrgUid"`
	VorUid       *VorUid     `xml:"VorUid" json:"VorUid"`
	TxtName      string      `xml:"txtName,omitempty" json:"txtName,omitempty"`
	CodeChannel  string      `xml:"codeChannel" validate:"dmeChan" json:"codeChannel"`
	ValGhostFreq string      `xml:"valGhostFreq,omitempty" validate:"omitempty,freq" json:"valGhostFreq,omitempty"`
	UomGhostFreq string      `xml:"uomGhostFreq,omitempty" validate:"omitempty,uomFreq" json:"uomGhostFreq,omitempty"`
	CodeDatum    string      `xml:"codeDatum" validate:"wge" json:"codeDatum"`
	ValElev      string      `xml:"valElev" validate:"omitempty,elev" json:"valElev"`
	UomDistVer   string      `xml:"uomDistVer" validate:"omitempty,uomDist" json:"uomDistVer"`
	Ttt          []Timetable `xml:"Ttt" validate:"dive" json:"Ttt"`
	TxtRmk       string      `xml:"txtRmk" json:"txtRmk"`
	validation
}

func (f *Tcn) Uid() FeatureUid {
	return &f.TcnUid
}

type VorUid struct {
	RegionalUid
	CodeId string `xml:"codeId" json:"codeId" validate:"codeId"`
	LatLong

	// TODO: check if correct
	validation
}

func (uid *VorUid) String() string {
	return UidString(*uid)
}

func (uid *VorUid) Hash() string {
	return UidHash(*uid)
}

type Vor struct {
	Source
	VorUid         VorUid      `xml:"VorUid" validate:"dive" json:"VorUid"`
	OrgUid         *OrgUid     `xml:"OrgUid" json:"OrgUid"`
	TxtName        string      `xml:"txtName,omitempty" json:"txtName,omitempty"`
	CodeType       string      `xml:"codeType" validate:"oneof=VOR DVOR OTHER" json:"codeType"`
	ValFreq        string      `xml:"valFreq" validate:"freq" json:"valFreq"`
	UomFreq        string      `xml:"uomFreq" validate:"eq=MHZ" json:"uomFreq"`
	CodeTypeNorth  string      `xml:"codeTypeNorth" validate:"oneof=TRUE MAG GRID UTM OTHER" json:"codeTypeNorth"`
	ValDeclination string      `xml:"valDeclination,omitempty" validate:"omitempty,numeric" json:"valDeclination,omitempty"`
	ValMagVar      string      `xml:"valMagVar,omitempty" validate:"omitempty,numeric" json:"valMagVar,omitempty"`
	DateMagVar     string      `xml:"dateMagVar,omitempty" validate:"omitempty,numeric" json:"dateMagVar,omitempty"`
	ValMagVarChg   string      `xml:"valMagVarChg,omitempty" validate:"omitempty,numeric" json:"valMagVarChg,omitempty"`
	CodeDatum      string      `xml:"codeDatum" validate:"wge" json:"codeDatum"`
	ValElev        string      `xml:"valElev,omitempty" validate:"omitempty,elev" json:"valElev,omitempty"`
	UomDistVer     string      `xml:"uomDistVer,omitempty" validate:"omitempty,uomDist" json:"uomDistVer,omitempty"`
	Vtt            []Timetable `xml:"Vtt,omitempty" validate:"dive" json:"Vtt,omitempty"`
	TxtRmk         string      `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Vor) Uid() FeatureUid {
	return &f.VorUid
}
