package undocumented

type AisMapDesignSet struct {
	XMLName                 string                    `xml:"aisMapDesignSet" json:"aisMapDesignSet"`
	Name                    string                    `xml:"name,attr" json:"name"`
	Origin                  string                    `xml:"origin,attr" json:"origin"`
	Created                 string                    `xml:"created,attr" json:"created"`
	Projection              string                    `xml:"projection,attr" json:"projection"`
	FlightInformationRegion string                    `xml:"flightInformationRegion,attr" json:"flightInformationRegion"`
	General                 AisMapDesignSetGeneral    `xml:"general" json:"general"`
	Airspaces               []AisMapDesignSetAirspace `xml:"airspace" json:"airspace"`
	// Airports
	Airports       []AisMapDesignSetAirport `xml:"airport" json:"airport"`
	Heliports      []AisMapDesignSetAirport `xml:"heliport" json:"heliport"`
	Ultralightport []AisMapDesignSetAirport `xml:"ultralightport" json:"ultralightport"`

	DVor         []AisMapDesignSetNav `xml:"DVOR" json:"DVOR"`
	Vor          []AisMapDesignSetNav `xml:"VOR" json:"VOR"`
	Ndb          []AisMapDesignSetNav `xml:"NDB" json:"NDB"`
	Dme          []AisMapDesignSetNav `xml:"DME" json:"DME"`
	Fix          []AisMapDesignSetNav `xml:"FIX" json:"FIX"`
	Tcn          []AisMapDesignSetNav `xml:"TCN" json:"TCN"`
	VfrGldr      []AisMapDesignSetNav `xml:"VFR-GLDR" json:"VFR-GLDR"`
	VfrRp        []AisMapDesignSetNav `xml:"VFR-RP" json:"VFR-RP"`
	VfrMrp       []AisMapDesignSetNav `xml:"VFR-MRP" json:"VFR-MRP"`
	VfrEnr       []AisMapDesignSetNav `xml:"VFR-ENR" json:"VFR-ENR"`
	MountainTop  []AisMapDesignSetNav `xml:"MOUNTAIN-TOP" json:"MOUNTAIN-TOP"`
	MountainPass []AisMapDesignSetNav `xml:"MOUNTAIN-PASS" json:"MOUNTAIN-PASS"`
	Town         []AisMapDesignSetNav `xml:"TOWN" json:"TOWN"`
}

type AisMapDesignSetGeneral struct {
	Appearance          AisMapDesignSetAppearance          `xml:"appearance" json:"appearance"`
	Grid                AisMapDesignSetGrid                `xml:"grid" json:"grid"`
	CountryBorders      AisMapDesignSetCountryBorders      `xml:"countryBorders" json:"countryBorders"`
	AirspaceGroundShade AisMapDesignSetAirspaceGroundShade `xml:"airspaceGroundShade" json:"airspaceGroundShade"`
}

type AisMapDesignSetAppearance struct {
	BackgroundColor string `xml:"backgroundcolor,attr" json:"backgroundcolor"`
}

type AisMapDesignSetGrid struct {
	Color string `xml:"color,attr" json:"color"`
	Width string `xml:"width,attr" json:"width"`
}

type AisMapDesignSetAirspaceGroundShade struct {
	Show  string `xml:"show,attr" json:"show"`
	Color string `xml:"color,attr" json:"color"`
}

type AisMapDesignSetCountryBorders struct {
	Show                       string `xml:"show,attr" json:"show"`
	DashStyle                  string `xml:"dashStyle,attr" json:"dashStyle"`
	SimplifyUnlessCorridorOfNm string `xml:"simplifyUnlessCorridorOfNm,attr" json:"simplifyUnlessCorridorOfNm"`
	MaxElementLengthOfNm       string `xml:"maxElementLengthOfNm,attr" json:"maxElementLengthOfNm"`
	Color                      string `xml:"color,attr" json:"color"`
}

type AisMapDesignSetAirspace struct {
	CodeType   string                        `xml:"codeType,attr" json:"codeType"`
	Descr      string                        `xml:"descr,attr,omitempty" json:"descr,omitempty"`
	Outline    AisMapDesignSetAirspaceStyle  `xml:"outline" json:"outline"`
	Halo       *AisMapDesignSetAirspaceStyle `xml:"halo" json:"halo"`
	Area       AisMapDesignSetAirspaceStyle  `xml:"area" json:"area"`
	Bend       *AisMapDesignSetAirspaceStyle `xml:"bend" json:"bend"`
	Designator *AisMapDesignSetDesignator    `xml:"designator" json:"designator"`
}

type AisMapDesignSetAirspaceStyle struct {
	Show      string `xml:"show,attr" json:"show"`
	Width     string `xml:"width,attr,omitempty" json:"width,omitempty"`
	Color     string `xml:"color,attr" json:"color"`
	DashStyle string `xml:"dashStyle,attr,omitempty" json:"dashStyle,omitempty"`
}

type AisMapDesignSetDesignator struct {
	// type="areaBox" show="1">
	Type                  string `xml:"type,attr,omitempty" json:"type,omitempty"`
	Show                  string `xml:"show,attr,omitempty" json:"show,omitempty"`
	BorderDockingDistance string `xml:"borderDockingDistance,attr,omitempty" json:"borderDockingDistance,omitempty"`
	Mode                  string `xml:"mode,attr,omitempty" json:"mode,omitempty"`

	// SVG HERE!
	Svg string `xml:",innerxml" json:"svg"`
}

type AisMapDesignSetAirport struct {
	Style string `xml:"style,attr" json:"style"`

	Icon       *AisMapDesignIcon         `xml:"icon" json:"icon"`
	Designator AisMapDesignSetDesignator `xml:"designator" json:"designator"`
}

type AisMapDesignSetNav struct {
	Style string `xml:"style,attr" json:"style"`

	Icon       *AisMapDesignIcon         `xml:"icon" json:"icon"`
	Designator AisMapDesignSetDesignator `xml:"designator" json:"designator"`
}

type AisMapDesignIcon struct {
	Svg string `xml:",innerxml" json:"svg"`
}

// Labels
type AisMapLabelPositioning struct {
	XMLName string        `xml:"aisMapLabelPositioning" json:"aisMapLabelPositioning"`
	Label   []AisMapLabel `xml:"label" json:"label"`
}

type AisMapLabel struct {
	Type  string `xml:"type,attr" json:"type"`
	UName string `xml:"uName,attr" json:"uName"`

	DeclutteringLayer []AisMapLabelDeclutteringLayer `xml:"declutteringLayer" json:"declutteringLayer"`
}

type AisMapLabelDeclutteringLayer struct {
	Id string `xml:"id,attr" json:"id"`

	Placement []AisMapLabelPlacement `xml:"placement" json:"placement"`
}

type AisMapLabelPlacement struct {
	TrackOrient    string `xml:"trackOrient,attr" json:"trackOrient"`
	X              string `xml:"x,attr" json:"x"`
	Y              string `xml:"y,attr" json:"y"`
	OutlinePolyIdx string `xml:"outlinePolyIdx,attr" json:"outlinePolyIdx"`
	Rotation       string `xml:"rotation,attr" json:"rotation"`
	YFocus         string `xml:"yFocus,attr" json:"yFocus"`
	XFocus         string `xml:"xFocus,attr" json:"xFocus"`
}
