package types

// TODO, still inconsistencies with documentation

type AseUid struct {
	RegionalUid
	CodeType     string `xml:"codeType" json:"codeType"` // validate:"required,oneof=FIR UIR CTA UTA TMA CTR TSA TRA AWY P R D RAS CLASS SECTOR D-OTHER"`
	CodeId       string `xml:"codeId" validate:"required,codeId" json:"codeId"`
	TxtLocalType string `xml:"txtLocalType,omitempty" validate:"omitempty" hash:"optional" json:"txtLocalType,omitempty"`
}

func (uid *AseUid) String() string {
	return UidString(*uid)
}

func (uid *AseUid) Hash() string {
	return UidHash(*uid)
}

func (uid *AseUid) Ref() *Ase {
	if uid.IRef == nil {
		// TODO, needed? BUGON
		return nil
	}
	return uid.IRef.(*Ase)
}

type Ase struct {
	Source
	AseUid           AseUid     `xml:"AseUid" json:"AseUid" validate:"dive"`
	OrgUid           *OrgUid    `xml:"OrgUid" json:"OrgUid"`
	TxtName          string     `xml:"txtName,omitempty" json:"txtName,omitempty"`
	CodeClass        string     `xml:"codeClass,omitempty" json:"codeClass,omitempty" validate:"omitempty,oneof=A B C D E F G"`
	CodeLocInd       string     `xml:"codeLocInd,omitempty" json:"codeLocInd,omitempty" validate:"omitempty,alpha,len=4"`
	CodeActivity     string     `xml:"codeActivity,omitempty" json:"codeActivity,omitempty" validate:"omitempty,oneof=ACROBAT AIRMODEL ANTIHAIL ARTILLERY ASCENT ATS BALLOON BLAST EXERCISE FAUNA GLIDER HANGGLIDER IND-NUCLEAR MILOPS NATURE NO-NOISE PARACHUTE PARAGLIDER SHOOT SPORT TFC-AD TFC-HELI TOWING TRG UAV ULM WINCH OTHER"`
	CodeDistVerUpper string     `xml:"codeDistVerUpper,omitempty" json:"codeDistVerUpper,omitempty" validate:"omitempty,oneof=HEI ALT STD"`
	ValDistVerUpper  string     `xml:"valDistVerUpper,omitempty" json:"valDistVerUpper,omitempty" validate:"omitempty,numeric"`
	UomDistVerUpper  string     `xml:"uomDistVerUpper,omitempty" json:"uomDistVerUpper,omitempty" validate:"omitempty,uomElev"`
	CodeDistVerLower string     `xml:"codeDistVerLower,omitempty" json:"codeDistVerLower,omitempty" validate:"omitempty,oneof=HEI ALT STD"`
	ValDistVerLower  string     `xml:"valDistVerLower,omitempty" json:"valDistVerLower,omitempty" validate:"omitempty,numeric"`
	UomDistVerLower  string     `xml:"uomDistVerLower,omitempty" json:"uomDistVerLower,omitempty" validate:"omitempty,uomElev"`
	CodeDistVerMax   string     `xml:"codeDistVerMax,omitempty" json:"codeDistVerMax,omitempty" validate:"omitempty,oneof=HEI ALT STD"`
	ValDistVerMax    string     `xml:"valDistVerMax,omitempty" json:"valDistVerMax,omitempty" validate:"omitempty,numeric"`
	UomDistVerMax    string     `xml:"uomDistVerMax,omitempty" json:"uomDistVerMax,omitempty" validate:"omitempty,uomElev"`
	CodeDistVerMnm   string     `xml:"codeDistVerMnm,omitempty" json:"codeDistVerMnm,omitempty" validate:"omitempty,oneof=HEI ALT STD"`
	ValDistVerMnm    string     `xml:"valDistVerMnm,omitempty" json:"valDistVerMnm,omitempty" validate:"omitempty,numeric"`
	UomDistVerMnm    string     `xml:"uomDistVerMnm,omitempty" json:"uomDistVerMnm,omitempty" validate:"omitempty,uomElev"`
	Att              *Timetable `xml:"Att,omitempty" json:"Att,omitempty" validate:"omitempty,dive"`
	CodeSelAvbl      string     `xml:"codeSelAvbl,omitempty" json:"codeSelAvbl,omitempty" validate:"omitempty,yesno"`
	TxtRmk           string     `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation

	// Relations
	Adg *Adg `xml:"-" json:"_Adg,omitempty" validate:"-"`
	Abd *Abd `xml:"-" json:"_Abd,omitempty" validate:"-"`
	Ser *Ser `xml:"-" json:"_Ser,omitempty" validate:"-"`
}

func (f *Ase) Uid() FeatureUid {
	return &f.AseUid
}

type AbdUid struct {
	Uid

	AseUid *AseUid `xml:"AseUid" json:"AseUid"`
}

func (uid *AbdUid) String() string {
	return UidString(*uid)
}

func (uid *AbdUid) Hash() string {
	return UidHash(*uid)
}

type Abd struct {
	AbdUid AbdUid `xml:"AbdUid"`
	Avx    []Avx  `xml:"Avx"`
	validation
}

func (f *Abd) Uid() FeatureUid {
	return &f.AbdUid
}

// Avx - Airspace Border Vertex
type Avx struct { // TODO
	GbrUid         *GbrUid `xml:"GbrUid,omitempty" json:"GbrUid,omitempty"`
	CodeType       string  `xml:"codeType" json:"codeType"`
	GeoLat         string  `xml:"geoLat" json:"geoLat"`
	GeoLong        string  `xml:"geoLong" json:"geoLong"`
	CodeDatum      string  `xml:"codeDatum" json:"codeDatum"`
	ValGeoAccuracy string  `xml:"valGeoAccuracy,omitempty" json:"valGeoAccuracy,omitempty"`
	UomGeoAccuracy string  `xml:"uomGeoAccuracy,omitempty" json:"uomGeoAccuracy,omitempty"`
	GeoLatArc      string  `xml:"geoLatArc,omitempty" json:"geoLatArc,omitempty"`
	GeoLongArc     string  `xml:"geoLongArc,omitempty" json:"geoLongArc,omitempty"`
	ValRadiusArc   string  `xml:"valRadiusArc,omitempty" json:"valRadiusArc,omitempty"`
	UomRadiusArc   string  `xml:"uomRadiusArc,omitempty" json:"uomRadiusArc,omitempty"`
	ValHex         string  `xml:"valHex,omitempty" json:"valHex,omitempty"`
	TxtRmk         string  `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

type AdgUid struct {
	// TODO, temp allow mid
	Uid

	AseUid *AseUid `xml:"AseUid" json:"AseUid"`
}

func (uid *AdgUid) String() string {
	return UidString(*uid)
}

func (uid *AdgUid) Hash() string {
	return UidHash(*uid)
}

type Adg struct {
	AdgUid           AdgUid  `xml:"AdgUid" json:"AdgUid"`
	AseUidSameExtent *AseUid `xml:"AseUidSameExtent" json:"AseUidSameExtent"`
	validation
}

func (f *Adg) Uid() FeatureUid {
	return &f.AdgUid
}

type SaeUid struct {
	Uid

	SerUid *SerUid `xml:"SerUid" json:"SerUid"`
	AseUid *AseUid `xml:"AseUid" json:"AseUid"`
}

func (uid *SaeUid) String() string {
	return UidString(*uid)
}

func (uid *SaeUid) Hash() string {
	return UidHash(*uid)
}

type Sae struct {
	SaeUid SaeUid `xml:"SaeUid"`
	validation
}

func (f *Sae) Uid() FeatureUid {
	return &f.SaeUid
}
