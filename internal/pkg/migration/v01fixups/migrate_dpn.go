package v01fixups

import (
	"log"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

func migrateDpn(t *types.Dpn) error {
	if t.CodeDatum == "" {
		t.CodeDatum = "WGE"
	}
	unkns := make([]types.XmlTag, 0)
	for _, v := range t.UnknownTags {
		switch v.XMLName.Local {
		case "codeTypeNorth":
			// drop, Dpn is not a Vor
		case "uomFreq":
			// drop, Dpn is not a Vor
		case "valMagVar":
			// drop, Dpn is not a Vor
		case "valMagVarChg":
			// drop, Dpn is not a Vor
		case "dateMagVar":
			// drop, Dpn is not a Vor
		case "valCrs":
			if t.ValTrueBrg == "" {
				t.ValTrueBrg = v.Content
			}
		case "valHgt":
			if t.ValElev == "" {
				t.ValElev = v.Content
			}
		case "uomDistVer":
			if t.UomElev == "" {
				t.UomElev = v.Content
			}
		default:
			log.Println("Dpn: invalid data:", v)
			unkns = append(unkns, v)
		}
	}
	// left-over fields
	t.UnknownTags = unkns
	if len(t.UnknownTags) == 0 {
		t.UnknownTags = nil
	}
	return nil
}
