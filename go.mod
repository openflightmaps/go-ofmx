module gitlab.com/openflightmaps/go-ofmx

go 1.19

require (
	github.com/StefanSchroeder/Golang-Ellipsoid v0.0.0-20221004092235-f00a9ab04789
	github.com/doublerebel/bellows v0.0.0-20160303004610-f177d92a03d3
	github.com/fatih/structtag v1.2.0
	github.com/go-playground/locales v0.14.1
	github.com/go-playground/universal-translator v0.18.0
	github.com/nqd/flat v0.2.0
	github.com/paulmach/go.geo v0.0.0-20180829195134-22b514266d33
	github.com/paulmach/go.geojson v1.4.0
	github.com/stretchr/testify v1.8.1
	golang.org/x/net v0.5.0
	gopkg.in/go-playground/validator.v9 v9.31.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/jstemmer/go-junit-report v1.0.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/text v0.6.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
