package xmlparserfixups

import (
	"encoding/xml"
	"io"
	"log"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
)

func New() xmlParserFixupMigrator {
	return xmlParserFixupMigrator{}
}

type xmlParserFixupMigrator struct{}

// This is needed because of a namespace issue with the go xml parser
func fixOfmxHeader(hdr *ofmx.OfmxHeader) {
	unknAttrs := make([]xml.Attr, 0)
	for _, v := range hdr.UnknownAttributes {
		switch v.Name.Local {
		case "xsi":
			hdr.Xsi = v.Value
		case "noNamespaceSchemaLocation":
			hdr.NoNamespaceSchemaLocation = v.Value
		default:
			log.Println("legacyFixup: invalid data:", v)
			unknAttrs = append(unknAttrs, v)
		}
	}
	// left-over fields
	hdr.UnknownAttributes = unknAttrs
	if len(hdr.UnknownAttributes) == 0 {
		hdr.UnknownAttributes = nil
	}
}

func (m xmlParserFixupMigrator) MigrateSnapshot(snap *ofmx.OfmxSnapshot) (*ofmx.OfmxSnapshot, error) {
	_, err := m.MigrateHeader(&snap.OfmxHeader)
	return snap, err
}

func (m xmlParserFixupMigrator) MigrateHeader(hdr *ofmx.OfmxHeader) (*ofmx.OfmxHeader, error) {
	fixOfmxHeader(hdr)
	return hdr, nil
}

func (m xmlParserFixupMigrator) MigrateReader(r io.Reader) (io.Reader, error) {
	return r, nil
}

func (m xmlParserFixupMigrator) CanMigrate(version string, hdr *ofmx.OfmxHeader) bool {
	return false
}

func (m xmlParserFixupMigrator) TargetVersion() string {
	return ""
}
