package test

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/utils"
	"golang.org/x/net/html/charset"
)

func TestExamplesMid(t *testing.T) {
	testcases := []string{
		"airport.ofmx",
		"airspace.ofmx",
		"label_marker.ofmx",
		"landing_aid.ofmx",
		"navigational_aid.ofmx",
		"obstacle.ofmx",
		"organisation.ofmx",
		"timetable.ofmx",
	}
	types.DefaultMidMode = types.MID_MODE_OFMX
	for _, tf := range testcases {
		f, err := os.Open("testdata/examples/" + tf)
		if err != nil {
			t.Error(err)
		}
		var snap ofmx.OfmxSnapshot
		decoder := xml.NewDecoder(f)
		decoder.Strict = true
		decoder.CharsetReader = charset.NewReaderLabel

		err = decoder.Decode(&snap)
		if err != nil {
			t.Error("Cannot parse input file:" + err.Error())
		}
		for _, f := range snap.Features.Slice() {
			uid := f.Uid()
			assert.Equal(t, uid.OriginalMid(), uid.Hash(), "mid mismatch:%s, file:%s", uid.String(), tf)
		}
	}
}

func AddToFmap(fmap types.FeatureMap, features []types.Feature) error {
	duplicates := make([]string, 0)
	for _, f := range features {
		_, err := fmap.ByUid(f.Uid())
		if err == nil {
			duplicates = append(duplicates, f.Uid().String())
			// TODO: this should be an error
			//nolint:forbidigo
			fmt.Printf("Duplicate feature: %s\n", f.Uid().String())
		}
		if err = fmap.AddFeatures(f); err != nil {
			return err
		}
	}
	if len(duplicates) == 0 {
		return nil
	} else {
		return errors.New("Duplicate features exist: " + strconv.Itoa(len(duplicates)))
	}
}

func TestXMLParser(t *testing.T) {
	testcases := []string{
		"airport.ofmx",
		"airspace.ofmx",
		"label_marker.ofmx",
		"landing_aid.ofmx",
		"navigational_aid.ofmx",
		"obstacle.ofmx",
		"organisation.ofmx",
		"timetable.ofmx",
	}
	fmap := types.NewFeatureMap()
	err := os.MkdirAll("out/", 0o750)
	assert.Nil(t, err)

	for _, n := range testcases {
		processFile(t, fmap, "testdata/examples/"+n, "out/"+n, true)
	}
	// update references after loading
	// ofmx.UpdateReferences(fmap)

	for _, n := range testcases {
		processFile(t, fmap, "testdata/examples/"+n, "out/"+n, false)
	}
}

func processFile(t *testing.T, fmap types.FeatureMap, inFile, outFile string, loadOnly bool) {
	var snap ofmx.OfmxSnapshot

	f, err := os.Open(inFile)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	decoder := ofmx.NewDecoder(f, ofmx.MidMode(types.MID_MODE_OFMX))

	err = decoder.Decode(&snap)
	if err != nil {
		t.Error(err)
		return
	}

	if loadOnly {
		err := AddToFmap(fmap, snap.Features.Slice())
		// TODO: Re-add
		_ = err
		// assert.Nil(t, err, "AddToFmap")
		return
	}

	o, err := os.CreateTemp(os.TempDir(), "ofmx-")
	if err != nil {
		log.Fatal("Cannot create temporary file", err)
	}
	defer o.Close()
	defer os.Remove(o.Name())

	encoder := xml.NewEncoder(o)
	encoder.Indent("", "  ")
	err = encoder.Encode(snap)
	if err != nil {
		log.Fatal("Cannot write output file:" + err.Error())
	}

	// re-parse XML
	_, err = f.Seek(0, io.SeekStart)
	assert.Nil(t, err)
	hashOriginal := utils.HashXML(f, true)

	// reload temp xml
	_, err = o.Seek(0, io.SeekStart)
	assert.Nil(t, err)
	hashRewritten := utils.HashXML(o, true)

	assert.Equal(t, len(hashOriginal), len(hashRewritten), "Number of nodes in XML do not match anymore")

	for i := range hashRewritten {
		assert.Equal(t, hashOriginal[i], hashRewritten[i], "Written XML doesn't match")
	}
}
