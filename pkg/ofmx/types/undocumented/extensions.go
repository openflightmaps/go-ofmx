package undocumented

import (
	"encoding/xml"
)

type OfmxShapeExtensions struct {
	// XMLName string
	Xsi                       string `xml:"xsi,attr" json:"xsi"`
	NoNamespaceSchemaLocation string `xml:"noNamespaceSchemaLocation,attr" json:"noNamespaceSchemaLocation"`

	Version   string `xml:"version,attr" json:"version"`
	Origin    string `xml:"origin,attr" json:"origin"`
	Namespace string `xml:"namespace,attr" json:"namespace"`
	Created   string `xml:"created,attr" json:"created"`
	Effective string `xml:"effective,attr" json:"effective"`

	Shapes ShapeMap `xml:",any" json:"shapes"`
}

type Shape struct {
	GmlPosList string `xml:"gmlPosList" json:"gmlPosList"`
}

type ShapeMap map[string]Shape

func (s ShapeMap) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	shape := Shape{}
	err := d.DecodeElement(&shape, &start)
	if err != nil {
		return err
	}
	s[start.Attr[0].Value] = shape
	return nil
}
