package types

// TODO: Redesign FeatureList implementation

type FeatureList interface {
	Slice() []Feature
	Append(features ...Feature) FeatureList
}
