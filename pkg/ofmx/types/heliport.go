package types

// Fato - Final approach and takeoff area

type FtoUid struct {
	Uid

	AhpUid   *AhpUid `xml:"AhpUid" json:"AhpUid" validate:"required" `
	TxtDesig string  `xml:"txtDesig" json:"txtDesig" validate:"textDesig"`
}

func (uid *FtoUid) String() string {
	return UidString(*uid)
}

func (uid *FtoUid) Hash() string {
	return UidHash(*uid)
}

// FATO
type Fto struct {
	FtoUid FtoUid `xml:"FtoUid" json:"FtoUid"`

	ValLen                 string `xml:"valLen,omitempty" json:"valLen,omitempty" validate:"numeric,omitempty"`
	ValWid                 string `xml:"valWid,omitempty" json:"valWid,omitempty" validate:"numeric,omitempty"`
	ValMaxDim              string `xml:"valMaxDim,omitempty" json:"valMaxDim,omitempty" validate:"omitempty,numeric"`
	UomDim                 string `xml:"uomDim,omitempty" json:"uomDim,omitempty" validate:"uomDist"`
	SurfaceCharacteristics `validate:"dive"`
	TxtProfile             string `xml:"txtProfile,omitempty" json:"txtProfile,omitempty"`
	TxtMarking             string `xml:"txtMarking,omitempty" json:"txtMarking,omitempty"`
	CodeSts                string `xml:"codeSts,omitempty" json:"codeSts,omitempty" validate:"omitempty,oneof=CLSD WIP PARKED FAILAID SPOWER OTHER"`
	TxtRmk                 string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	// TODO
	// ValMaxRotorDia         string `xml:"valMaxRotorDia,omitempty" validate:"omitempty,numeric" json:"valMaxRotorDia,omitempty"`
	// XXXValMaxRotorDim string `xml:"valMaxRotorDim,omitempty" validate:"omitempty,undocumented" json:"valMaxRotorDim,omitempty"`
	// maxRotorDia
	validation
}

func (f *Fto) Uid() FeatureUid {
	return &f.FtoUid
}

// Fato Direction

type FdnUid struct {
	// TODO, temp allow mid
	Uid

	FtoUid   *FtoUid `xml:"FtoUid" json:"FtoUid"`
	TxtDesig string  `xml:"txtDesig" json:"txtDesig"`
}

func (uid *FdnUid) String() string {
	return UidString(*uid)
}

func (uid *FdnUid) Hash() string {
	return UidHash(*uid)
}

type Fdn struct {
	FdnUid FdnUid `xml:"FdnUid" json:"FdnUid"`

	// migrate
	// XXXrearTakeOffSectorAvail      string `xml:"rearTakeOffSectorAvail,omitempty" validate:"undocumented" json:"-"`
	// XXXrearTakeoffSectorBrg        string `xml:"rearTakeoffSectorBrg,omitempty" validate:"undocumented" json:"-"`
	// XXXrearTakeoffSectorSlopeAngle string `xml:"rearTakeoffSectorSlopeAngle,omitempty" validate:"undocumented" json:"-"`
	// XXXcodingMode                  string `xml:"codingMode,omitempty" validate:"undocumented" json:"-"`
	// XXXiapInitialAlt          string `xml:"iapInitialAlt,omitempty" validate:"undocumented" json:"-"`

	// XXXgeoLat  string `xml:"geoLat,omitempty" validate:"undocumented" json:"-"`
	// XXXgeoLong string `xml:"geoLong,omitempty" validate:"undocumented" json:"-"`

	// normal
	ValTrueBrg                     string     `xml:"valTrueBrg,omitempty" json:"valTrueBrg,omitempty"`
	ValMagBrg                      string     `xml:"valMagBrg,omitempty" json:"valMagBrg,omitempty"`
	CodeTypeVasis                  string     `xml:"codeTypeVasis,omitempty" json:"codeTypeVasis,omitempty"`
	CodePsnVasis                   string     `xml:"codePsnVasis,omitempty" json:"codePsnVasis,omitempty"`
	NoBoxVasis                     string     `xml:"noBoxVasis,omitempty" json:"noBoxVasis,omitempty"`
	CodePortableVasis              string     `xml:"codePortableVasis,omitempty" json:"codePortableVasis,omitempty"`
	ValSlopeAngleGpVasis           string     `xml:"valSlopeAngleGpVasis,omitempty" json:"valSlopeAngleGpVasis,omitempty"`
	ValMeht                        string     `xml:"valMeht,omitempty" json:"valMeht,omitempty"`
	UomMeht                        string     `xml:"uomMeht,omitempty" json:"uomMeht,omitempty"`
	CodeRearTakeOffSectorAvbl      string     `xml:"codeRearTakeOffSectorAvbl,omitempty" validate:"omitempty,yesno" json:"codeRearTakeOffSectorAvbl,omitempty"`
	ValRearTakeOffSectorTrueBrg    string     `xml:"valRearTakeOffSectorTrueBrg,omitempty" validate:"omitempty,numeric,len=3" json:"valRearTakeOffSectorTrueBrg,omitempty"`
	ValRearTakeOffSectorSlopeAngle string     `xml:"valRearTakeOffSectorSlopeAngle,omitempty" validate:"omitempty,numeric" json:"valRearTakeOffSectorSlopeAngle,omitempty"`
	CodeCodingMode                 string     `xml:"codeCodingMode,omitempty" validate:"omitempty,oneof=CONST PARA" json:"codeCodingMode,omitempty"`
	GeoLatIap                      string     `xml:"geoLatIap,omitempty" validate:"omitempty,lat" json:"geoLatIap,omitempty"`
	GeoLongIap                     string     `xml:"geoLongIap,omitempty" validate:"omitempty,lon" json:"geoLongIap,omitempty"`
	ValAltIap                      string     `xml:"valAltIap,omitempty" validate:"omitempty,numeric" json:"valAltIap,omitempty"`
	UomAltIap                      string     `xml:"uomAltIap,omitempty" validate:"omitempty,uomDist" json:"uomAltIap,omitempty"`
	BezierSectorExtent             BezierPath `xml:"bezierSectorExtent,omitempty" json:"bezierSectorExtent,omitempty"`
	BezierSectorCentreLine         BezierPath `xml:"bezierSectorCentreLine,omitempty" json:"bezierSectorCentreLine,omitempty"`
	TxtRmk                         string     `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation

	// XXXgmlSectorCenterline *BezierPathGmlPosList `xml:"gmlSectorCenterline,omitempty" validate:"undocumented" json:"-"`
	// XXXgmlSectorExtend     *BezierPathGmlPosList `xml:"gmlSectorExtend,omitempty" validate:"undocumented" json:"-"`

	// TODO, document
	XXXnonIcaoValSlopeAngle   string `xml:"nonIcaoValSlopeAngle,omitempty" validate:"undocumented" json:"-"`
	XXXnonIcaoValOpeningAngle string `xml:"nonIcaoValOpeningAngle,omitempty" validate:"undocumented" json:"-"`
	XXXnonIcaoMaxSectorLen    string `xml:"nonIcaoMaxSectorLen,omitempty" validate:"undocumented" json:"-"`

	// XXXgmlPosList          string              `xml:"gmlPosList,omitempty" validate:"undocumented" json:"-"`
}

// type XXXgmlSectorExtend struct {
// 	XXXgmlPosList []string `xml:"gmlPosList,omitempty" validate:"undocumented" json:"gmlPosList,omitempty"`
// }

type (
	BezierPath           string
	BezierPathGmlPosList struct {
		GmlPosList BezierPath `xml:"gmlPosList,omitempty" json:"gmlPosList,omitempty"`
	}
)

func (f *Fdn) Uid() FeatureUid {
	return &f.FdnUid
}

// Fato Direction Declared Distance

type FddUid struct {
	// TODO, temp allow mid
	Uid

	FdnUid        FdnUid `xml:"FdnUid" json:"FdnUid"`
	CodeType      string `xml:"codeType" json:"codeType"`
	CodeDayPeriod string `xml:"codeDayPeriod" json:"codeDayPeriod"`
}

func (uid *FddUid) String() string {
	return UidString(*uid)
}

func (uid *FddUid) Hash() string {
	return UidHash(*uid)
}

type Fdd struct {
	FddUid FddUid `xml:"FddUid" json:"FddUid"`

	ValDist string `xml:"valDist" json:"valDist"`
	UomDist string `xml:"uomDist" json:"uomDist"`
	TxtRmk  string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Fdd) Uid() FeatureUid {
	return &f.FddUid
}

// Helipad (TLOF)

type TlaUid struct {
	Uid

	AhpUid   *AhpUid `xml:"AhpUid" json:"AhpUid"`
	TxtDesig string  `xml:"txtDesig" json:"txtDesig"`
}

func (uid *TlaUid) String() string {
	return UidString(*uid)
}

func (uid *TlaUid) Hash() string {
	return UidHash(*uid)
}

type Tla struct {
	TlaUid    TlaUid `xml:"TlaUid" json:"TlaUid"`
	FtoUid    FtoUid `xml:"FtoUid" json:"FtoUid"`
	GeoLat    string `xml:"geoLat" json:"geoLat"`
	GeoLong   string `xml:"geoLong" json:"geoLong"`
	CodeDatum string `xml:"codeDatum" json:"codeDatum"`
	// TODO, fix
	XXXvalTrueBrg string `xml:"valTrueBrg,omitempty" json:"valTrueBrg,omitempty"`

	ValElev    string `xml:"valElev,omitempty" json:"valElev,omitempty"`
	UomDistVer string `xml:"uomDistVer,omitempty" json:"uomDistVer,omitempty"`
	ValLen     string `xml:"valLen,omitempty" json:"valLen,omitempty"`
	ValWid     string `xml:"valWid,omitempty" json:"valWid,omitempty"`
	UomDim     string `xml:"uomDim,omitempty" json:"uomDim,omitempty"`
	ValSlope   string `xml:"valSlope,omitempty" json:"valSlope,omitempty" validate:"undocumented"`
	SurfaceCharacteristics
	CodeClassHel string `xml:"codeClassHel,omitempty" json:"codeClassHel,omitempty"`
	TxtMarking   string `xml:"txtMarking,omitempty" json:"txtMarking,omitempty"`
	CodeSts      string `xml:"codeSts,omitempty" json:"codeSts,omitempty"`
	TxtRmk       string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Tla) Uid() FeatureUid {
	return &f.TlaUid
}

// Helipad (TLOF) Lightning

type TlsUid struct {
	Uid

	TlaUid  *TlaUid `xml:"TlaUid" json:"TlaUid"`
	CodePsn string  `xml:"codePsn" json:"codePsn"`
}

func (uid *TlsUid) String() string {
	return UidString(*uid)
}

func (uid *TlsUid) Hash() string {
	return UidHash(*uid)
}

type Tls struct {
	TlsUid TlsUid `xml:"TlsUid" json:"TlsUid"`

	TxtDescr   string `xml:"txtDescr,omitempty" json:"txtDescr,omitempty"`
	CodeIntst  string `xml:"codeIntst,omitempty" json:"codeIntst,omitempty"`
	CodeColour string `xml:"codeColour,omitempty" json:"codeColour,omitempty"`
	TxtRmk     string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Tls) Uid() FeatureUid {
	return &f.TlsUid
}

// Fda - FATO Direction Approach Lighting

type FdaUid struct {
	Uid
	FdnUid   *FdnUid `xml:"FdnUid" json:"FdnUid"`
	CodeType string  `xml:"codeType" json:"codeType"`
}

func (uid *FdaUid) String() string {
	return UidString(*uid)
}

func (uid *FdaUid) Hash() string {
	return UidHash(*uid)
}

type Fda struct {
	FdaUid             FdaUid `xml:"FdaUid" json:"FdaUid"`
	ValLen             string `xml:"valLen,omitempty" json:"valLen,omitempty"`
	UomLen             string `xml:"uomLen,omitempty" json:"uomLen,omitempty"`
	CodeIntst          string `xml:"codeIntst,omitempty" json:"codeIntst,omitempty"`
	CodeSequencedFlash string `xml:"codeSequencedFlash,omitempty" json:"codeSequencedFlash,omitempty"`
	TxtDescrFlash      string `xml:"txtDescrFlash,omitempty" json:"txtDescrFlash,omitempty"`
	TxtRmk             string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Fda) Uid() FeatureUid {
	return &f.FdaUid
}

// Fls - FATO Direction Lighting

type FlsUid struct {
	Uid
	FdnUid  *FdnUid `xml:"FdnUid" json:"FdnUid"`
	CodePsn string  `xml:"codePsn" json:"codePsn"`
}

func (uid *FlsUid) String() string {
	return UidString(*uid)
}

func (uid *FlsUid) Hash() string {
	return UidHash(*uid)
}

type Fls struct {
	FlsUid     FlsUid `xml:"FlsUid" json:"FlsUid"`
	TxtDescr   string `xml:"txtDescr,omitempty" json:"txtDescr,omitempty"`
	CodeIntst  string `xml:"codeIntst,omitempty" json:"codeIntst,omitempty"`
	CodeColour string `xml:"codeColour,omitempty" json:"codeColour,omitempty"`
	TxtRmk     string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (f *Fls) Uid() FeatureUid {
	return &f.FlsUid
}
