package undocumented

import (
	"fmt"
	"strings"

	geo "github.com/paulmach/go.geo"
	geojson "github.com/paulmach/go.geojson"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

func (f *Twy) GeoJson(fmap types.FeatureMap) (*geojson.Feature, error) {
	return f.XtSurface.geoJson(fmap)
}

func (f *Apn) GeoJson(fmap types.FeatureMap) (*geojson.Feature, error) {
	return f.XtSurface.geoJson(fmap)
}

func (f *Msc) GeoJson(fmap types.FeatureMap) (*geojson.Feature, error) {
	return f.XtSurface.geoJson(fmap)
}

func (f *XtSurface) geoJson(fmap types.FeatureMap) (*geojson.Feature, error) {
	return gmlGeoJson(f.GmlPosList)
}

func (f *Tly) GeoJson(fmap types.FeatureMap) (*geojson.Feature, error) {
	return f.XtSurface.geoJson(fmap)
}

func (f *Prc) GeoJson(fmap types.FeatureMap) (*geojson.Feature, error) {
	return f.BezTrajectory.geoJson(fmap)
}

func (f *GmlPath) geoJson(fmap types.FeatureMap) (*geojson.Feature, error) {
	return gmlGeoJson(f.GmlPosList)
}

//nolint:unused
func gmlGeometry(gmlString string) (*geojson.Geometry, error) {
	g, err := gmlGeoJson(gmlString)
	if err != nil {
		return nil, err
	}
	return g.Geometry, nil
}

func gmlGeoJson(gmlString string) (*geojson.Feature, error) {
	psn := strings.Split(gmlString, " ")
	path := geo.NewPath()
	for i := 0; i < len(psn); i++ {
		longlat := strings.Split(psn[i], ",")
		if len(longlat) == 2 {
			pt, err := types.ParseLongLat(longlat[0], longlat[1], "WGE")
			if err != nil {
				return nil, err
			}
			path.Push(pt)
		}
	}
	if len(path.PointSet) < 2 {
		return nil, fmt.Errorf("invalid path")
	}
	p := path.ToGeoJSON()
	return p, nil
}
