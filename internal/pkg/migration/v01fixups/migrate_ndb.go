package v01fixups

import (
	"log"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

func migrateNdb(t *types.Ndb) error {
	unkns := make([]types.XmlTag, 0)
	for _, v := range t.UnknownTags {
		switch v.XMLName.Local {
		case "codeTypeNorth":
			// Ndb: drop codeTypeNorth, not a VOR
		default:
			log.Println("Ndb: invalid data:", v)
			unkns = append(unkns, v)
		}
	}
	// left-over fields
	t.UnknownTags = unkns
	if len(t.UnknownTags) == 0 {
		t.UnknownTags = nil
	}
	return nil
}
