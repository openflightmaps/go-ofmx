package internal

import (
	"fmt"
	"log"
	"reflect"
	"strings"
)

func IntHash(s reflect.Value, all bool, _midModeOFMX bool) []string {
	val := make([]string, 0)
	t := s.Type()
	switch t.Kind() {
	case reflect.Ptr:
		ptr := s.Elem()
		if !s.IsValid() || s.IsZero() {
			return val
		}
		return IntHash(ptr, all, _midModeOFMX)
	case reflect.Interface:
		ptr := s.Elem()
		if s.IsZero() {
			return val
		}
		return IntHash(ptr, all, _midModeOFMX)
	case reflect.Slice:
		// TODO, slice?
		for i := 0; i < s.Len(); i++ {
			val = append(val, IntHash(s.Index(i), all, _midModeOFMX)...)
		}
		return val
	case reflect.Struct:
		for i := 0; i < s.NumField(); i++ {
			t := s.Type().Field(i)
			f := s.Field(i)
			h := t.Tag.Get("hash")
			if h == "ignore" {
				continue
			}
			if h == "optional" && !all {
				continue
			}

			name := strings.Split(t.Tag.Get("xml"), ",")[0]
			if _midModeOFMX && name != "" {
				val = append(val, name)
			}
			switch f.Kind() {
			case reflect.String:
				val = append(val, f.String())
			case reflect.Int:
				val = append(val, fmt.Sprintf("%d", f.Int()))
			case reflect.Struct:
				val = append(val, IntHash(f, all, _midModeOFMX)...)
			case reflect.Ptr:
				val = append(val, IntHash(f, all, _midModeOFMX)...)
			case reflect.Slice:
				val = append(val, IntHash(f, all, _midModeOFMX)...)
			case reflect.Interface:
				val = append(val, IntHash(f, all, _midModeOFMX)...)
			default:
				log.Println(t)
				log.Println(f.Kind())
				panic("ERROR in IntHash")
			}
		}
	default:
		log.Println(s)
	}
	return val
}
