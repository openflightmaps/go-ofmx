package ofmx

import (
	"encoding/xml"
	"io"
	"time"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

// <OFMX-Snapshot version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://schema.openflightmaps.org/0/OFMX-Snapshot.xsd" effective="2019-11-28T13:32:23" origin="ofmx editor" created="2019-11-28T13:32:23" namespace="210444d1-4576-e92d-0983-4669182a8c04">

func updateSnapshot(snap *OfmxSnapshot) {
	snap.Xsi = "http://www.w3.org/2001/XMLSchema-instance"
	snap.NoNamespaceSchemaLocation = "https://schema.openflightmaps.org/0.1/OFMX-Snapshot.xsd"
	snap.Version = "0.1"
	snap.Origin = "go-ofmx/0.1"
}

func NewOFMXSnapshot() *OfmxSnapshot {
	now := types.XmlTime(time.Now().UTC())

	snap := OfmxSnapshot{}
	snap.Created = &now
	snap.Effective = &now

	updateSnapshot(&snap)
	return &snap
}

type encoderConfig struct {
	Pretty bool
}

type Encoder struct {
	encoder *xml.Encoder
	encoderConfig
}

type EncoderOption func(*encoderConfig)

func NewEncoder(w io.Writer, options ...EncoderOption) *Encoder {
	// TODO: default false?
	config := encoderConfig{Pretty: true}
	for _, o := range options {
		o(&config)
	}
	enc := Encoder{}
	enc.encoder = xml.NewEncoder(w)
	if config.Pretty {
		enc.encoder.Indent("", "   ")
	}
	return &enc
}

func (enc *Encoder) Encode(snap *OfmxSnapshot) error {
	// updateSnapshot(snap)
	return enc.encoder.Encode(snap)
}

func PrettyPrint(b bool) EncoderOption {
	return func(c *encoderConfig) {
		c.Pretty = b
	}
}
