List of changes for v01fixups / 05.01.2023

* [ ] Ase/Att: codeDay/codeDayTil: replace HOL with LH
* [ ] TODO: Ase: rename txtLocalType to ...
* [ ] Ase: move invalid codeType's to txtLocalType
* [ ] Ase: codeSelAvbl: replace True, False, 0, 1 with Y/N
* [ ] Ase: move xt_txtRmk into txtRmk
* [ ] Ase: move xt_selAvail into codeSelAvbl
* [ ] Dpn: drop fields: uomFreq, valMagVar, valMagVarChg, dateMagVar
* [ ] Dpn: Fields renamed:
  * [ ] valCrs to valTrueBrg
  * [ ] valHgt to valElev
  * [ ] uomDistVer to uomElev
* [ ] Ndb: Remove codeTypeNorth
* [ ] Fdn: Fields renamed:
  * [ ] codingMode to codeCodingMode
  * [ ] rearTakeOffSectorAvail to codeRearTakeOffSectorAvbl
  * [ ] rearTakeoffSectorBrg to valRearTakeOffSectorTrueBrg
  * [ ] rearTakeoffSectorSlopeAngle to valRearTakeOffSectorSlopeAngle
  * [ ] valSlope to valSlopeAngleGpVasis
  * [ ] iapInitialAlt to valAltIap, set uomAltIap to FT
  * [ ] geoLat, geoLong to geoLatIap, geoLongIap
  * [ ] TODO: nonIcaoValSlopeAngle, nonIcaoValOpeningAngle, nonIcaoMaxSectorLen
* [ ] Fdn: format changes
  * [ ] gmlSectorExtend moved to bezierSectorExtent (removing inner gmlPosList)
  * [ ] gmlSectorCenterline moved to bezierSectorCentreLine (removing inner gmlPosList element)
* [ ] Prc changes (undocumented)
  * [ ] Leg entry/exit reference:
    1. [ ] use codeId instead of txtName
    2. [ ] move codeType up into entry/exit node
    3. [ ] move tfcDir up into entry/exit node
    4. [ ] FIXED-POS codeType: add corresponding Dpn entry to Prc snapshot or add full Dpn instead of DpnUid
    5. [ ] RdnUid is reversed (AhpUid->RwyUid->RdnUid instead of RdnUid->RwyUid->AhpUid)
    6. [ ] region property missing on some Uids
  * [ ] PrcUid
    * [ ] Mandatory link only to AhpUid, fix incorrect AhpUid object
    * [ ] TODO: Add link to RdnUid in Prc body as RdnUidAssoc ?