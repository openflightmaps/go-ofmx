package validation

import (
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"gitlab.com/openflightmaps/go-ofmx/internal/pkg/validation"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gopkg.in/go-playground/validator.v9"
	en_translations "gopkg.in/go-playground/validator.v9/translations/en"
)

type Validator struct {
	validate *validator.Validate
	trans    ut.Translator
}

func New() *Validator {
	validate := validator.New()
	enLang := en.New()
	uni := ut.New(enLang, enLang)
	trans, _ := uni.GetTranslator("en")
	if err := en_translations.RegisterDefaultTranslations(validate, trans); err != nil {
		panic(err)
	}

	if err := validation.RegisterValidators(validate, trans); err != nil {
		panic(err)
	}

	return &Validator{
		validate: validate,
		trans:    trans,
	}
}

func (val *Validator) ValidateFeature(f types.Feature) []ValidationError {
	errorList := make([]ValidationError, 0)

	err := val.validate.Struct(f)
	if err != nil {
		// errs := err.(validator.ValidationErrors)
		// fmt.Println(errs)

		for _, err := range err.(validator.ValidationErrors) {
			errorList = append(errorList, ValidationError{
				Id:    f.Uid().String(),
				Field: err.StructNamespace(),
				Value: err.Value(),
				Tag:   err.Tag(),
				Error: err.Translate(val.trans),
				// Error: err.Translate(trans),
			})
		}

		// log.Printf("validation failed: %v", err)
	}
	return errorList
}

type ValidationError struct {
	Id    string
	Field string
	Tag   string
	Error string
	Value interface{}
}
