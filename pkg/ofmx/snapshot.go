package ofmx

import (
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

// <OFMX-Snapshot version="1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="https://schema.openflightmaps.org/0/OFMX-Snapshot.xsd" effective="2019-11-28T13:32:23" origin="ofmx editor" created="2019-11-28T13:32:23" namespace="210444d1-4576-e92d-0983-4669182a8c04">

type OfmxHeader struct {
	XMLName                   string         `xml:"OFMX-Snapshot"`
	Xsi                       string         `xml:"xmlns:xsi,attr"`
	NoNamespaceSchemaLocation string         `xml:"xsi:noNamespaceSchemaLocation,attr"`
	Version                   string         `xml:"version,attr"`
	Origin                    string         `xml:"origin,attr"`
	Namespace                 string         `xml:"namespace,attr,omitempty"`
	Regions                   string         `xml:"regions,attr,omitempty"`
	Sourced                   *types.XmlTime `xml:"sourced,attr"`
	Created                   *types.XmlTime `xml:"created,attr"`
	Effective                 *types.XmlTime `xml:"effective,attr"`
	Expiration                *types.XmlTime `xml:"expiration,attr,omitempty"`

	// validation
	validation
}
type AixmHeader struct {
	XMLName string `xml:"AIXM-Snapshot"`
}

type ParseError struct {
	Errors  []error
	Message string
}

func (e ParseError) Error() string {
	return e.Message
}

type OfmxSnapshot struct {
	OfmxHeader
	Features featureList `xml:",any"`

	// decoder state and config
	decoderConfig *decoderConfig `xml:"-" validate:"-"`
}

func (snap *OfmxSnapshot) setDecoderConfig(c *decoderConfig) {
	snap.decoderConfig = c
	// TODO: hack, remove
	types.IgnoreUndocumented = c.IgnoreUndocumented
	types.DefaultMidMode = c.MidMode
}

type AixmSnapshot struct {
	AixmHeader
	Features featureList `xml:",any"`

	// decoder state and config
	decoderConfig *decoderConfig `xml:"-" validate:"-"`
}

func (snap *AixmSnapshot) setDecoderConfig(c *decoderConfig) {
	snap.decoderConfig = c
	// TODO: hack, remove
	types.IgnoreUndocumented = c.IgnoreUndocumented
	types.DefaultMidMode = c.MidMode
}
