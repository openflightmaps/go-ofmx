package types

// Geographical border types
type GbrUid struct {
	Uid
	TxtName string `xml:"txtName" validate:"required" json:"txtName"`
}

type Gbr struct {
	GbrUid   GbrUid `xml:"GbrUid" validate:"dive" json:"GbrUid"`
	CodeType string `xml:"codeType" json:"codeType"` // TODO validate:"required"`
	TxtRmk   string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	Gbv      []Gbv  `xml:"Gbv" validate:"dive" json:"Gbv"`
	validation
}

type Gbv struct {
	CodeType       string `xml:"codeType" validate:"oneof=GRC END" json:"codeType"`
	GeoLat         string `xml:"geoLat" validate:"lat" json:"geoLat"`
	GeoLong        string `xml:"geoLong" validate:"lon" json:"geoLong"`
	CodeDatum      string `xml:"codeDatum" json:"codeDatum"` // TODO validate:"required,wge"`
	ValGeoAccuracy string `xml:"valGeoAccuracy,omitempty" json:"valGeoAccuracy,omitempty"`
	UomGeoAccuracy string `xml:"uomGeoAccuracy,omitempty" json:"uomGeoAccuracy,omitempty"`
	GeoLatArc      string `xml:"geoLatArc,omitempty" json:"geoLatArc,omitempty"`
	GeoLongArc     string `xml:"geoLongArc,omitempty" json:"geoLongArc,omitempty"`
	ValRadiusArc   string `xml:"valRadiusArc,omitempty" json:"valRadiusArc,omitempty"`
	UomRadiusArc   string `xml:"uomRadiusArc,omitempty" json:"uomRadiusArc,omitempty"`
	ValHex         string `xml:"valHex,omitempty" json:"valHex,omitempty"`
	TxtRmk         string `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

func (uid *GbrUid) String() string {
	return UidString(*uid)
}

func (uid *GbrUid) Hash() string {
	return UidHash(*uid)
}

func (f *Gbr) Uid() FeatureUid {
	return &f.GbrUid
}
