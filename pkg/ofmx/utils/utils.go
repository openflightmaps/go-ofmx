package utils

import (
	"encoding/xml"
	"io"
	"sort"
	"strings"

	ofmx "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"golang.org/x/net/html/charset"
)

type Node struct {
	XMLName xml.Name
	Attrs   []xml.Attr `xml:"-"`
	Content []byte     `xml:",chardata"`
	Nodes   []Node     `xml:",any"`
}

func (n *Node) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	n.Attrs = start.Attr
	type node Node

	return d.DecodeElement((*node)(n), &start)
}

func HashXML(r io.Reader, sorted bool) []string {
	ofmx.DefaultMidMode = ofmx.MID_MODE_OFMX
	var d []Node
	xmlDecoder := xml.NewDecoder(r)
	xmlDecoder.Strict = true
	xmlDecoder.CharsetReader = charset.NewReaderLabel

	err := xmlDecoder.Decode(&d)
	if err != nil {
		panic("Cannot parse input file:" + err.Error())
	}

	data := make([]*[]string, 0)
	var current *[]string

	walker := func(node Node, level int) bool {
		// ignore empty node
		if len(node.Nodes) == 0 && len(node.Attrs) == 0 && string(node.Content) == "" {
			return true
		}

		// if current != nil {
		// 	fmt.Print(strings.Join(*current, "|"), " ")
		// }
		// fmt.Println(level, node.XMLName.Local, string(node.Content))
		if sorted && level > 0 {
			// sort nodes, except on top node (snapshot feature order)
			sort.SliceStable(node.Nodes, func(i, j int) bool {
				return node.Nodes[i].XMLName.Local < node.Nodes[j].XMLName.Local
			})
		}
		if level == 0 {
		} else if level == 1 {
			c := make([]string, 0)
			current = &c
			*current = append(*current, node.XMLName.Local)

			data = append(data, current)
		} else {
			*current = append(*current, node.XMLName.Local)
			if len(node.Attrs) > 0 {
				sort.Slice(node.Attrs, func(i, j int) bool {
					return node.Attrs[i].Name.Local < node.Attrs[j].Name.Local
				})
				for _, x := range node.Attrs {

					if x.Name.Local == "mid" { // skip mid
						continue
					}
					if x.Name.Local == "newEntity" { // skip newEntity
						continue
					}
					*current = append(*current, x.Name.Local)
					*current = append(*current, x.Value)
				}
			}
			if len(node.Nodes) == 0 {
				*current = append(*current, string(node.Content))
				// fmt.Println(string(node.Content))
			}
		}
		return true
	}

	walk(d, 0, walker)

	list := make([]string, 0)

	for _, x := range data {
		hash := strings.Join(*x, "|")
		list = append(list, hash)
	}

	sort.SliceStable(list, func(i, j int) bool {
		return list[i] < list[j]
	})

	return list
}

func walk(nodes []Node, level int, f func(Node, int) bool) {
	for _, n := range nodes {
		if f(n, level) {
			walk(n.Nodes, level+1, f)
		}
	}
}
