package v01fixups

import (
	"fmt"
	"log"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

// TODO: rename txtLocalType
// TODO: fix invalid codeTypes->move to txtLocalType
// AseUid|ED|CTR|ANSBACH: codeDay must be one of [MON TUE WED THU FRI SAT SUN WD PWD AWD LH PLH ALH ANY] 'HOL'
// AseUid|ED|CTR|ANSBACH: codeDayTil must be one of [MON TUE WED THU FRI SAT SUN WD PWD AWD LH PLH ALH ANY] 'HOL'
// TODO: codeSelAvail YESNO
// Key: 'Ase.codeSelAvbl' Error:Field validation for 'codeSelAvbl' failed on the 'yesno' tag 'True'

func migrateAse(t *types.Ase) error {
	if t.CodeSelAvbl == "True" {
		t.CodeSelAvbl = "Y"
	} else if t.CodeSelAvbl == "False" {
		t.CodeSelAvbl = "N"
	}

	if t.Att != nil {
		for i, att := range t.Att.Timsh {
			if att.CodeDay == "HOL" {
				t.Att.Timsh[i].CodeDay = "LH"
			}
			if att.CodeDayTil == "HOL" {
				t.Att.Timsh[i].CodeDayTil = "LH"
			}
		}
	}

	unkns := make([]types.XmlTag, 0)
	for _, v := range t.UnknownTags {
		switch v.XMLName.Local {
		case "xt_selAvail":
			if v.Content == "True" || v.Content == "1" {
				t.CodeSelAvbl = "Y"
				continue
			}
			if v.Content == "False" || v.Content == "0" {
				t.CodeSelAvbl = "N"
				continue
			}
			return fmt.Errorf("invalid value for xt_selAvail: %s", v.Content)
		case "xt_txtRmk":
			if t.TxtRmk == "" {
				t.TxtRmk = v.Content
			}
			// TODO: Find solution

			continue
			// log.Printf("cannot migrate xt_txtRmk, TxtRmk is already set (%s)", t.TxtRmk)

			// return fmt.Errorf("cannot migrate xt_txtRmk, TxtRmk is already set (%s)", t.TxtRmk)
		case "txtLocalType":
			// TODO: HACK, remove
			// t.AseUid.TxtLocalType = strings.ReplaceAll(v.Content, " ", "")
		default:
			log.Println("Ase: invalid data:", v)
			unkns = append(unkns, v)
		}
	}
	// left-over fields
	t.UnknownTags = unkns
	if len(t.UnknownTags) == 0 {
		t.UnknownTags = nil
	}
	return nil
}
