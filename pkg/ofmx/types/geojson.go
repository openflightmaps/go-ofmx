package types

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"

	"github.com/StefanSchroeder/Golang-Ellipsoid/ellipsoid"
	flatten "github.com/nqd/flat"
	geo "github.com/paulmach/go.geo"
	geojson "github.com/paulmach/go.geojson"
)

func parseCoord(in string) (float64, error) {
	if len(in) == 0 {
		return 0, nil
	}
	v := in[:len(in)-1]
	d := string(in[len(in)-1])

	dir := 1.0
	if d == "W" || d == "S" {
		dir = -1.0
	}
	f, _ := strconv.ParseFloat(v, 64)

	if f > 180 {
		log.Println("invalid coord:" + in)
		return f * dir, errors.New("invalid coord:" + in)
	}
	return f * dir, nil
}

func ParseLongLat(long, lat string, codeDatum string) (*geo.Point, error) {
	if codeDatum != "" && codeDatum != "WGE" {
		return nil, errors.New("Unsupported codeDatum: " + codeDatum)
	}
	longp, err := parseCoord(long)
	if err != nil {
		return nil, err
	}
	latp, err := parseCoord(lat)
	if err != nil {
		return nil, err
	}
	p := geo.NewPoint(longp, latp)
	return p, nil
}

type latLongGeoJson LatLong

func (ll latLongGeoJson) toGeoJson(fmap FeatureMap, codeDatum string) (*geojson.Feature, error) {
	pt, err := ParseLongLat(ll.GeoLong, ll.GeoLat, codeDatum)
	if err != nil {
		return nil, err
	}
	p := pt.ToGeoJSON()
	return p, nil
}

// point-based geometries
// navigation
func (f *Dpn) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	return latLongGeoJson(f.DpnUid.LatLong).toGeoJson(fmap, f.CodeDatum)
}

func (f *Dme) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	return latLongGeoJson(f.DmeUid.LatLong).toGeoJson(fmap, f.CodeDatum)
}

func (f *Mkr) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	return latLongGeoJson(f.MkrUid.LatLong).toGeoJson(fmap, f.CodeDatum)
}

func (f *Ndb) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	return latLongGeoJson(f.NdbUid.LatLong).toGeoJson(fmap, f.CodeDatum)
}

func (f *Tcn) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	return latLongGeoJson(f.TcnUid.LatLong).toGeoJson(fmap, f.CodeDatum)
}

func (f *Vor) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	return latLongGeoJson(f.VorUid.LatLong).toGeoJson(fmap, f.CodeDatum)
}

// AD
func (f *Ahp) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	return latLongGeoJson(f.LatLong).toGeoJson(fmap, f.CodeDatum)
}

// Label Marker
func (f *Lbm) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	return latLongGeoJson(f.LatLong).toGeoJson(fmap, f.CodeDatum)
}

// helper
func getNearestPointOffset(pt *geo.Point, points *geo.Path) int {
	nd := math.Inf(1)
	// var np geo.Point
	var ni int = 0
	for i, p := range points.PointSet {
		d := p.DistanceFrom(pt)
		if d < nd {
			nd = d
			// np = p
			ni = i
		}
	}
	return ni
}

// Airspace

func reversePointSet(a geo.PointSet) {
	for i := len(a)/2 - 1; i >= 0; i-- {
		opp := len(a) - 1 - i
		a[i], a[opp] = a[opp], a[i]
	}
}

func getSlice(pts geo.PointSet, start, end int) geo.PointSet {
	if start <= end {
		return pts[start:end]
	} else {
		b := append(geo.PointSet(nil), pts[end:start]...)
		reversePointSet(b)
		return b
	}
}

// TODO: only circles supported at the moment
func circleToPolygon(centerPoint, startPoint, endPoint *geo.Point, sections int, ccw bool) ([]geo.Point, error) {
	points := make([]geo.Point, 0, sections+1)

	geof := ellipsoid.Init("WGS84", ellipsoid.Degrees, ellipsoid.Meter, ellipsoid.LongitudeIsSymmetric, ellipsoid.BearingNotSymmetric)
	radius, _ := geof.To(centerPoint[1], centerPoint[0], endPoint[1], endPoint[0])

	for i := 0.0; i <= float64(sections); i++ {
		adjust := i / float64(sections)
		lat, long := geof.At(centerPoint[1], centerPoint[0], radius, adjust*360.0)
		points = append(points, geo.Point{long, lat})
	}

	return points, nil
}

// returs a great circle or rhumb line from a to b, not including a and b, generated every 0.5 degree
func drawLine(a, b *geo.Point, ps *geo.PointSet, grc bool) {
	if math.Abs(a.Lat()-b.Lat()) < 0.5 {
		return
	}
	var mid *geo.Point
	mp := geo.NewLine(a, b)
	if grc {
		mid = mp.GeoMidpoint()
	} else {
		log.Println("DEBUG: RHUMB LINE")
		mid = mp.Midpoint()
	}
	drawLine(a, mid, ps, grc)
	ps.Push(mid)
	drawLine(mid, b, ps, grc)
}

func (f *Abd) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	poly := geo.NewPath()
	var queued *geo.Path
	var start int
	var prevPsn *geo.Point

	log.Println(f.Uid().String())

	// TODO, fixup non-closed Avx first
	//
	if f.Avx == nil || len(f.Avx) == 0 {
		return nil, fmt.Errorf("Abd contains empty Avx")
	}
	{
		first := f.Avx[0]
		firstPsn, err := ParseLongLat(first.GeoLong, first.GeoLat, first.CodeDatum)
		if err != nil {
			log.Println(firstPsn)
			return nil, err
		}

		last := f.Avx[len(f.Avx)-1]
		lastPsn, err := ParseLongLat(last.GeoLong, last.GeoLat, last.CodeDatum)
		if err != nil {
			log.Println(lastPsn)
			return nil, err
		}

		if lastPsn.String() != firstPsn.String() {
			log.Println("Abd is not a closed shape! FIXING UP!", f.Uid().String(), lastPsn.String(), firstPsn.String())
			avx := Avx{
				CodeType:  "GRC",
				CodeDatum: first.CodeDatum,
				GeoLat:    first.GeoLat,
				GeoLong:   first.GeoLong,
			}
			f.Avx = append(f.Avx, avx)
		}
	}

	for _, avx := range f.Avx {
		psn, err := ParseLongLat(avx.GeoLong, avx.GeoLat, avx.CodeDatum)
		if err != nil {
			log.Println(psn)
			return nil, err
		}
		// log.Println(avx.CodeType, avx.GbrUid, psn)

		// TODO fallback for circles
		if prevPsn == nil {
			prevPsn = psn
		}

		if queued != nil {
			end := getNearestPointOffset(psn, queued)
			// oints from %d to %d", start, end)
			poly.PointSet = append(poly.PointSet, getSlice(queued.PointSet, start, end)...)
			// TODO
			//*poly = append(poly, geom.LineString...)
			queued = nil
		}

		switch avx.CodeType {
		case "GRC":
			drawLine(prevPsn, psn, &poly.PointSet, true)
			poly.Push(psn)
		case "RHL":
			drawLine(prevPsn, psn, &poly.PointSet, false)
			poly.Push(psn)
		case "FNT": // BORDER
			f, err := fmap.ByUid(avx.GbrUid)
			if err != nil {
				return nil, errors.New("GBR not found:" + avx.GbrUid.String())
			}
			gbr := f.(*Gbr)
			queued, err = gbr.GeoJsonPath(fmap)
			if err != nil {
				return nil, err
			}
			start = getNearestPointOffset(psn, queued)
			// log.Println("Starting at", start, avx.GbrUid.String())

		case "CWA":
			if len(f.Avx) != 1 {
				return nil, fmt.Errorf("Currently only full circles supported: %s", f.AbdUid.String())
			}
			centerPsn, err := ParseLongLat(avx.GeoLongArc, avx.GeoLatArc, avx.CodeDatum)
			if err != nil {
				return nil, err
			}
			geom, err := circleToPolygon(centerPsn, prevPsn, psn, 36, false)
			if err != nil {
				return nil, err
			}
			for _, g := range geom {
				poly.Push(&g)
			}
		case "CCA":
			log.Println("Error: UNKNOWN CodeType: " + avx.CodeType)
			log.Println(avx)
			return nil, fmt.Errorf("unknown CodeType %s for  %s", avx.CodeType, f.AbdUid.String())

			// TODO, silly circle
			// poly.Push(psn)
		default:
			log.Println("UNKNOWN CodeType: " + avx.CodeType)
		}
		prevPsn = psn
	}

	p := toGeoJSONPolygon(poly)
	return p, nil
}

func (f *Abd) GeoJsonGeometry(fmap FeatureMap) (*geojson.Geometry, error) {
	gj, err := f.GeoJson(fmap)
	if err != nil {
		return nil, err
	}
	return gj.Geometry, err
}

func (f *Ase) GeoJsonGeometry(fmap FeatureMap) (*geojson.Geometry, error) {
	aseUid := &f.AseUid
	// find airspace derived geometry
	adgUid := &AdgUid{AseUid: aseUid}
	adg, err := fmap.ByUid(adgUid)
	if err == nil {
		a := adg.(*Adg)
		aseUid = a.AseUidSameExtent
	}

	// find airspace border AbdUid
	abdUid := &AbdUid{AseUid: aseUid}
	abd, err := fmap.ByUid(abdUid)
	if err == nil {
		gf := abd.(GeoFeatureGeometry)
		return gf.GeoJsonGeometry(fmap)
	}

	return nil, fmt.Errorf("geometry not found for Ase %s", f.AseUid.String())
}

// Geographical Borders

func (f *Gbr) GeoJsonPath(fmap FeatureMap) (*geo.Path, error) {
	poly := geo.NewPath()
	var firstPsn *geo.Point
	for _, gbv := range f.Gbv {
		switch gbv.CodeType {
		case "GRC":
			psn, err := ParseLongLat(gbv.GeoLong, gbv.GeoLat, gbv.CodeDatum)
			if err != nil {
				return nil, err
			}
			if firstPsn == nil {
				firstPsn = psn
			}
			poly.Push(psn)
		case "END":
			psn, err := ParseLongLat(gbv.GeoLong, gbv.GeoLat, gbv.CodeDatum)
			if err != nil {
				return nil, err
			}
			poly.Push(psn)

			// TODO: fix documentation
			// if psn.String() != firstPsn.String() {
			// log.Println("last and first point mismatch", f.GbrUid.TxtName, psn.String(), firstPsn.String())
			// TODO throw error
			// return nil, fmt.Errorf("last and first point mismatch for %s", f.GbrUid.TxtName)
			// }
			// TODO: verify if it matches start
			// log.Printf("CodeType End: %s\n", gbv)
		default:
			log.Println("UNKNOWN CodeType: " + gbv.CodeType)
			// TODO, drop hack
			psn, err := ParseLongLat(gbv.GeoLong, gbv.GeoLat, gbv.CodeDatum)
			if err == nil {
				poly.Push(psn)
			}
		}
	}
	return poly, nil
}

// ToGeoJSON creates a new geojson feature with a linestring geometry
// containing all the points.
func toGeoJSONPolygon(p *geo.Path) *geojson.Feature {
	coords := make([][]float64, 0, len(p.PointSet))

	for _, p := range p.PointSet {
		coords = append(coords, []float64{p[0], p[1]})
	}

	return geojson.NewPolygonFeature([][][]float64{coords})
}

func (f *Gbr) GeoJson(fmap FeatureMap) (*geojson.Feature, error) {
	g, err := f.GeoJsonPath(fmap)
	if err != nil {
		return nil, err
	}
	gf := toGeoJSONPolygon(g)
	return gf, err
}

func FillProperties(f Feature, gf *geojson.Feature) error {
	js, err := json.Marshal(f)
	if err != nil {
		return err
	}
	var fjs map[string]interface{}
	err = json.Unmarshal(js, &fjs)
	if err != nil {
		return err
	}
	// drop Avx, hack TODO
	delete(fjs, "_Avx")
	// TODO, drop Avx instead of whole _Abd
	delete(fjs, "_Abd")

	// flatten structure to e.g. AseUid.CodeId
	props, err := flatten.Flatten(fjs, nil)
	if err != nil {
		return err
	}
	// TODO, improve
	// use object name from UID e.g. Ase
	n := strings.Split(f.Uid().String(), "|")
	props["_type"] = strings.Replace(n[0], "Uid", "", 1)

	gf.Properties = props
	return nil
}

type GeoFeature interface {
	Uid() FeatureUid // From Feature
	GeoJson(fmap FeatureMap) (*geojson.Feature, error)
}

type GeoFeatureGeometry interface {
	Uid() FeatureUid // From Feature
	GeoJsonGeometry(fmap FeatureMap) (*geojson.Geometry, error)
}
