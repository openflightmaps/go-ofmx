package undocumented

import "gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"

type ApnUid struct {
	types.Uid

	AhpUid  *types.AhpUid `xml:"AhpUid" json:"AhpUid"`
	TxtName string        `xml:"txtName" json:"txtName"`
}

func (uid *ApnUid) String() string {
	return types.UidString(*uid)
}

func (uid *ApnUid) Hash() string {
	return types.UidHash(*uid)
}

type Apn struct {
	ApnUid ApnUid `xml:"ApnUid" json:"ApnUid"`
	types.SurfaceCharacteristics
	TxtRmk    string    `xml:"txtRmk" json:"txtRmk"`
	XtSurface XtSurface `xml:"xt_surface" validate:"dive" json:"xt_surface"`

	// TODO document
	XtLabel []XtLabel `xml:"xt_label" validate:"dive" json:"xt_label"`
}

func (f *Apn) Uid() types.FeatureUid {
	return &f.ApnUid
}
