package types

type LatLong struct {
	GeoLat  string `xml:"geoLat" json:"geoLat" validate:"lat"`
	GeoLong string `xml:"geoLong" json:"geoLong" validate:"lon"`
}
