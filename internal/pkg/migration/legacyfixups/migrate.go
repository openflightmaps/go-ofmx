package legacyfixups

import (
	"io"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

func New() legacyFixupMigrator {
	return legacyFixupMigrator{}
}

type legacyFixupMigrator struct{}

func (m legacyFixupMigrator) MigrateSnapshot(snap *ofmx.OfmxSnapshot) (*ofmx.OfmxSnapshot, error) {
	for _, f := range snap.Features.Slice() {
		var err error
		switch t := f.(type) {
		case *types.Gbr:
			err = migrateGbr(t)
		}
		if err != nil {
			return nil, err
		}
	}
	return snap, nil
}

func (m legacyFixupMigrator) MigrateReader(r io.Reader) (io.Reader, error) {
	return r, nil
}

func (m legacyFixupMigrator) CanMigrate(version string, hdr *ofmx.OfmxHeader) bool {
	if version != "" || hdr == nil {
		return false
	}
	if hdr.Version == "4.5" {
		return true
	}
	if hdr.Version == "1.0" {
		return true
	}
	if hdr.Version == "1.0 " {
		return true
	}
	// if header.Version == "4.5" && targetVersion == "0.1" {
	// 	return v01fixups.New(), nil
	// }
	// if header.Version == "1.0 " && targetVersion == "0.1" {
	// 	return v01fixups.New(), nil
	// }
	// if header.Version == "1.0" && targetVersion == "0.1" {
	// 	return v01fixups.New(), nil
	// }
	return false
}

func (m legacyFixupMigrator) TargetVersion() string {
	return "0.1-pre"
}
