package types

// Ils Instrument Landing System

type IlsUid struct {
	Uid
	RdnUid *RdnUid `xml:"RdnUid" json:"RdnUid"`
	// TODO complete
	// FdnUid *FdnUid `xml:"FdnUid,omitempty"`
}

func (uid *IlsUid) String() string {
	return UidString(*uid)
}

func (uid *IlsUid) Hash() string {
	return UidHash(*uid)
}

type Ils struct {
	IlsUid  IlsUid  `xml:"IlsUid" json:"IlsUid"`
	DmeUid  *DmeUid `xml:"DmeUid" json:"DmeUid"`
	CodeCat string  `xml:"codeCat" json:"codeCat"`
	TxtRmk  string  `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	Ilz     Ilz     `xml:"Ilz" json:"Ilz"`
	Igp     *Igp    `xml:"Igp" json:"Igp"`
	validation
}

func (f *Ils) Uid() FeatureUid {
	return &f.IlsUid
}

// Ilz Localizer

type Ilz struct {
	CodeId          string      `xml:"codeId" json:"codeId"`
	ValFreq         string      `xml:"valFreq" json:"valFreq"`
	UomFreq         string      `xml:"uomFreq" json:"uomFreq"`
	CodeTypeUseBack string      `xml:"codeTypeUseBack,omitempty" json:"codeTypeUseBack,omitempty"`
	GeoLat          string      `xml:"geoLat" json:"geoLat"`
	GeoLong         string      `xml:"geoLong" json:"geoLong"`
	CodeDatum       string      `xml:"codeDatum" json:"codeDatum"`
	ValElev         string      `xml:"valElev,omitempty" json:"valElev,omitempty"`
	UomDistVer      string      `xml:"uomDistVer,omitempty" json:"uomDistVer,omitempty"`
	Ilt             []Timetable `xml:"Ilt" json:"Ilt"`
	TxtRmk          string      `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}

// Igp Glide Path

type Igp struct {
	ValFreq    string      `xml:"valFreq" json:"valFreq"`
	UomFreq    string      `xml:"uomFreq" json:"uomFreq"`
	ValSlope   string      `xml:"valSlope,omitempty" json:"valSlope,omitempty"`
	ValRdh     string      `xml:"valRdh,omitempty" json:"valRdh,omitempty"`
	UomRdh     string      `xml:"uomRdh,omitempty" json:"uomRdh,omitempty"`
	GeoLat     string      `xml:"geoLat" json:"geoLat"`
	GeoLong    string      `xml:"geoLong" json:"geoLong"`
	CodeDatum  string      `xml:"codeDatum" json:"codeDatum"`
	ValElev    string      `xml:"valElev,omitempty" json:"valElev,omitempty"`
	UomDistVer string      `xml:"uomDistVer,omitempty" json:"uomDistVer,omitempty"`
	Igt        []Timetable `xml:"Igt" json:"Igt"`
	TxtRmk     string      `xml:"txtRmk,omitempty" json:"txtRmk,omitempty"`
	validation
}
