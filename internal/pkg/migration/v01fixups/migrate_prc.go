package v01fixups

import (
	"encoding/xml"
	"log"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types/undocumented"
)

func migratePrcUid(l *undocumented.LegNode, t *undocumented.PrcUid) error {
	unknAttrs := make([]xml.Attr, 0)
	for _, v := range t.UnknownAttributes {
		switch v.Name.Local {
		case "region":
			t.AhpUid.Region = v.Value
		default:
			log.Println("PrcUid: invalid data:", v)
			unknAttrs = append(unknAttrs, v)
		}
	}

	unkns := make([]undocumented.XmlTag, 0)
	for _, v := range t.UnknownTags {
		switch v.XMLName.Local {
		case "txtName":
			t.CodeId = v.Content
		case "codeType":
			// move to leg node
			if l == nil {
				log.Println("dropping codeType on PrcUid")
				continue
			}
			l.CodeType = v.Content
		case "tfcDir":
			// move to leg node
			if l == nil {
				log.Println("dropping tfcDir on PrcUid")
				continue
			}
			l.TfcDir = v.Content
		default:
			log.Println("PrcUid: invalid data:", v)
			unkns = append(unkns, v)
		}
	}
	// left-over fields
	t.UnknownTags = unkns
	if len(t.UnknownTags) == 0 {
		t.UnknownTags = nil
	}
	// left-over fields
	t.UnknownAttributes = unknAttrs
	if len(t.UnknownAttributes) == 0 {
		t.UnknownAttributes = nil
	}
	return nil
}

func migrateDpnUid(l *undocumented.LegNode, t *types.DpnUid) error {
	unkns := make([]types.XmlTag, 0)
	for _, v := range t.UnknownTags {
		switch v.XMLName.Local {
		case "txtName":
			t.CodeId = v.Content
		case "codeType":
			// move to leg node
			l.CodeType = v.Content
		default:
			log.Println("DpnUid:invalid data:", v)
			unkns = append(unkns, v)
		}
	}
	// left-over fields
	t.UnknownTags = unkns
	if len(t.UnknownTags) == 0 {
		t.UnknownTags = nil
	}
	return nil
}

func migrateVorUid(l *undocumented.LegNode, t *types.VorUid) error {
	unkns := make([]types.XmlTag, 0)
	for _, v := range t.UnknownTags {
		switch v.XMLName.Local {
		case "txtName":
			t.CodeId = v.Content
		case "codeType":
			// move to leg node
			l.CodeType = v.Content
		default:
			log.Println("VorUid: invalid data:", v)
			unkns = append(unkns, v)
		}
	}
	// left-over fields
	t.UnknownTags = unkns
	if len(t.UnknownTags) == 0 {
		t.UnknownTags = nil
	}
	return nil
}

func migrateNdbUid(l *undocumented.LegNode, t *types.NdbUid) error {
	unkns := make([]types.XmlTag, 0)
	for _, v := range t.UnknownTags {
		switch v.XMLName.Local {
		case "txtName":
			t.CodeId = v.Content
		case "codeType":
			// move to leg node
			l.CodeType = v.Content
		default:
			log.Println("NdbUid: invalid data:", v)
			unkns = append(unkns, v)
		}
	}
	// left-over fields
	t.UnknownTags = unkns
	if len(t.UnknownTags) == 0 {
		t.UnknownTags = nil
	}
	return nil
}

func migrateLegNode(t *undocumented.LegNode, region string) error {
	// TODO: fix Rdn encoding, it's currently reversed
	// Fix Dpn, for "SET POS"
	// where to store type of Point?
	if t.DpnUid != nil {
		if err := migrateDpnUid(t, t.DpnUid); err != nil {
			return err
		}
	}
	if t.VorUid != nil {
		if err := migrateVorUid(t, t.VorUid); err != nil {
			return err
		}
	}
	if t.PrcUid != nil {
		if err := migratePrcUid(t, t.PrcUid); err != nil {
			return err
		}
	}
	if t.NdbUid != nil {
		if err := migrateNdbUid(t, t.NdbUid); err != nil {
			return err
		}
	}
	return nil
}

func migratePrc(t *undocumented.Prc) error {
	if err := migratePrcUid(nil, &t.PrcUid); err != nil {
		return err
	}
	region := t.PrcUid.AhpUid.Region

	// TODO: Remove hack
	if t.UsageType == "" {
		t.UsageType = "FIXED_WING"
	}

	if t.CodeType == "TRAFFIC_CIRCUIT" {
		t.Leg = nil
	}
	for i, l := range t.Leg {
		if l.Type == "" {
			// TODO: Remove hack
			t.Leg[i].Type = "FIX_TO_FIX"
		}
		if err := migrateLegNode(l.Entry, region); err != nil {
			return err
		}
		if err := migrateLegNode(l.Exit, region); err != nil {
			return err
		}
	}

	return nil
}
