package v01fixups

import (
	"encoding/xml"
	"fmt"
	"log"
	"strconv"

	"gitlab.com/openflightmaps/go-ofmx/pkg/ofmx/types"
)

func migrateFdn(t *types.Fdn) error {
	unkns := make([]types.XmlTag, 0)
	for _, v := range t.UnknownTags {
		switch v.XMLName.Local {
		case "codingMode":
			t.CodeCodingMode = v.Content
		case "rearTakeOffSectorAvail":
			t.CodeRearTakeOffSectorAvbl = v.Content
		case "rearTakeoffSectorBrg":
			i, err := strconv.ParseInt(v.Content, 10, 64)
			if err != nil {
				return fmt.Errorf("could not migrate rearTakeoffSectorBrg value: " + v.Content)
			}
			s := fmt.Sprintf("%.3d", i)
			t.ValRearTakeOffSectorTrueBrg = s
		case "rearTakeoffSectorSlopeAngle":
			t.ValRearTakeOffSectorSlopeAngle = v.Content
		// case "nonIcaoValSlopeAngle": // ??
		// 	t. = v.Content
		// case "nonIcaoValOpeningAngle":
		// 	t.nonIcaoValOpeningAngle = v.Content
		// case "nonIcaoMaxSectorLen":
		// 	t.nonIcaoMaxSectorLen = v.Content
		case "valSlope":
			t.ValSlopeAngleGpVasis = v.Content
		case "iapInitialAlt":
			t.ValAltIap = v.Content
			t.UomAltIap = "FT"
		case "geoLat":
			t.GeoLatIap = v.Content
		case "geoLong":
			t.GeoLongIap = v.Content
		case "gmlSectorExtend":
			bez := types.BezierPathGmlPosList{}
			err := xml.Unmarshal([]byte(v.Content), &bez)
			if err != nil {
				return fmt.Errorf("unable to decode gmlSectorExtend")
			}
			t.BezierSectorExtent = bez.GmlPosList
		case "gmlSectorCenterline":
			bez := types.BezierPathGmlPosList{}
			err := xml.Unmarshal([]byte(v.Content), &bez)
			if err != nil {
				return fmt.Errorf("unable to decode gmlSectorCenterline")
			}
			t.BezierSectorCentreLine = bez.GmlPosList
		default:
			log.Println("Fdn: invalid data:", v)
			unkns = append(unkns, v)
		}
	}
	// left-over fields
	t.UnknownTags = unkns
	if len(t.UnknownTags) == 0 {
		t.UnknownTags = nil
	}
	return nil
}
